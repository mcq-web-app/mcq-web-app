-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2024 at 04:17 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcq-web-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `name`, `subject_id`, `created_at`, `updated_at`) VALUES
(1, 'Introduction to Software Engineering', 1, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(2, 'Models in SEN', 1, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(3, 'TESTING AND MAINTENANCE', 1, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(4, 'Introduction to 8086 Architecture', 2, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(5, 'Instruction Set and Programming', 2, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(6, 'Instruction Set and Programming', 2, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(7, 'Introduction to Java Basics', 3, '2020-10-01 19:09:51', '2020-10-01 19:09:51'),
(8, 'Working with Data Structures', 3, '2020-10-01 19:09:52', '2020-10-01 19:09:52'),
(9, 'Start with JDBC', 3, '2020-10-01 19:09:52', '2020-10-01 19:09:52'),
(10, 'Introduction to Programming', 4, '2020-10-01 19:09:52', '2020-10-01 19:09:52'),
(11, 'Introduction to compiler , Linker and loader', 4, '2020-10-01 19:09:52', '2020-10-01 19:09:52'),
(12, 'Constants, Variables and Data types in C', 4, '2020-10-01 19:09:52', '2020-10-01 19:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_14_102125_create_subjects_table', 1),
(5, '2020_08_14_102202_create_chapters_table', 1),
(6, '2020_08_14_102239_create_questions_table', 1),
(7, '2020_08_14_103001_create_options_table', 1),
(8, '2020_09_01_090753_create_students_table', 1),
(9, '2020_09_05_174608_create_tests_table', 1),
(10, '2020_09_06_082448_create_practise_tests_table', 1),
(11, '2020_09_23_142014_practise_test_results_table', 1),
(12, '2020_09_30_211631_create_scheduled_tests_table', 1),
(13, '2020_10_02_003828_create_scheduled_test_results_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` bigint(20) UNSIGNED NOT NULL,
  `chapter_id` bigint(20) UNSIGNED NOT NULL,
  `option` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `question_id`, `subject_id`, `chapter_id`, `option`, `correct`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Tempora necessitatibus accusamus ut.', 0, '2020-09-12 05:53:20', '2020-10-03 10:27:03'),
(2, 1, 1, 1, 'Aut quo ea voluptate veniam voluptatibus quae.', 0, '2020-09-12 05:53:20', '2020-10-03 10:27:03'),
(3, 1, 1, 1, 'Voluptatum labore placeat autem.', 0, '2020-09-12 05:53:20', '2020-10-03 10:27:03'),
(4, 1, 1, 1, 'updated this is correct!', 1, '2020-09-12 05:53:21', '2020-10-03 10:27:03'),
(5, 2, 1, 1, 'Ad quidem quo sint aliquam doloribus placeat.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(6, 2, 1, 1, 'Et laborum ullam sunt expedita earum.', 1, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(7, 2, 1, 1, 'Sed nam reprehenderit dolores earum.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(8, 2, 1, 1, 'Voluptate odio quia qui expedita deleniti.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(9, 3, 1, 1, 'Voluptatibus dolorem eos quidem fugiat voluptatum voluptatum ut quibusdam.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(10, 3, 1, 1, 'Nostrum voluptas quia hic.', 1, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(11, 3, 1, 1, 'Vero qui assumenda dolor sit aut sit.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(12, 3, 1, 1, 'Pariatur animi consequatur et.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(13, 4, 1, 1, 'Aperiam reprehenderit expedita dolor et.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(14, 4, 1, 1, 'Molestiae ea cumque reprehenderit consequuntur eveniet.', 1, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(15, 4, 1, 1, 'Nesciunt in omnis quas reiciendis.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(16, 4, 1, 1, 'Fuga deserunt excepturi aliquam voluptatem temporibus culpa sunt.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(17, 5, 1, 1, 'Quaerat et vel nihil tempora omnis dolor.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(18, 5, 1, 1, 'Officiis consequuntur aliquam autem inventore voluptas et.', 1, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(19, 5, 1, 1, 'Labore impedit tempore molestias velit nostrum cupiditate.', 0, '2020-09-12 05:53:21', '2020-09-12 05:53:21'),
(20, 5, 1, 1, 'Suscipit qui quasi fugit quibusdam.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(21, 6, 1, 1, 'Corporis saepe id ullam ipsa id.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(22, 6, 1, 1, 'Blanditiis sequi iusto quidem.', 1, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(23, 6, 1, 1, 'Omnis non laboriosam dolorem.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(24, 6, 1, 1, 'Cum nemo asperiores deleniti.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(25, 7, 1, 1, 'Deleniti ut fuga sint sint.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(26, 7, 1, 1, 'Odio beatae delectus quas architecto odit sit.', 1, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(27, 7, 1, 1, 'Est eum voluptates reprehenderit est vel tempore.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(28, 7, 1, 1, 'Asperiores qui a accusamus nostrum.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(29, 8, 1, 1, 'Veritatis aut totam ullam reprehenderit velit.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(30, 8, 1, 1, 'Quisquam repudiandae nostrum quia repellendus.', 1, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(31, 8, 1, 1, 'Est velit voluptatem voluptas amet aspernatur consectetur excepturi.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(32, 8, 1, 1, 'Est eligendi est blanditiis.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(33, 9, 1, 1, 'Impedit adipisci dolore rem qui.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(34, 9, 1, 1, 'Excepturi ipsum vel et.', 1, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(35, 9, 1, 1, 'Similique pariatur et est corrupti corporis voluptas.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(36, 9, 1, 1, 'Ut necessitatibus natus veniam.', 0, '2020-09-12 05:53:22', '2020-09-12 05:53:22'),
(37, 10, 1, 1, 'Ut velit id enim minima rem ipsam sapiente.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(38, 10, 1, 1, 'Enim amet incidunt.', 1, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(39, 10, 1, 1, 'Et enim aut sed et.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(40, 10, 1, 1, 'Mollitia illo aut repellendus ex libero aut.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(41, 11, 1, 2, 'Dolores corrupti dolores cumque suscipit et.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(42, 11, 1, 2, 'Ipsum voluptas doloremque in.', 1, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(43, 11, 1, 2, 'Dolore blanditiis non tenetur dolore voluptatem.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(44, 11, 1, 2, 'Sint eos repellat quos quos.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(45, 12, 1, 2, 'Eos neque dolor corporis consequatur.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(46, 12, 1, 2, 'Consectetur aliquam veniam id.', 1, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(47, 12, 1, 2, 'Officiis qui nulla commodi facilis.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(48, 12, 1, 2, 'Explicabo rerum reprehenderit dolore aliquid.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(49, 13, 1, 2, 'Ex debitis aliquam libero adipisci hic assumenda rerum.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(50, 13, 1, 2, 'Odit deserunt hic fugiat.', 1, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(51, 13, 1, 2, 'Dolorem itaque distinctio ea odit excepturi fugiat.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(52, 13, 1, 2, 'Minima molestias ut sapiente.', 0, '2020-09-12 05:53:23', '2020-09-12 05:53:23'),
(53, 14, 1, 2, 'Voluptatibus labore quod qui pariatur dolores asperiores.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(54, 14, 1, 2, 'Assumenda voluptatem aut id.', 1, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(55, 14, 1, 2, 'Impedit repellat fugit dolor aut consectetur doloremque.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(56, 14, 1, 2, 'Ut ipsam laboriosam.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(57, 15, 1, 2, 'Illo consequatur qui laboriosam.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(58, 15, 1, 2, 'Autem numquam nostrum tempora accusantium quidem.', 1, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(59, 15, 1, 2, 'Non eos et quos facilis esse ratione.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(60, 15, 1, 2, 'Quod similique repudiandae sint.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(61, 16, 1, 2, 'Aperiam officiis dolores tempora quasi.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(62, 16, 1, 2, 'Cupiditate facilis natus aperiam et.', 1, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(63, 16, 1, 2, 'Laudantium eum rerum est ut non.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(64, 16, 1, 2, 'Fuga qui sit voluptatem aspernatur omnis possimus aut cum.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(65, 17, 1, 2, 'At exercitationem id impedit.', 0, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(66, 17, 1, 2, 'Quas ut at autem voluptate.', 1, '2020-09-12 05:53:24', '2020-09-12 05:53:24'),
(67, 17, 1, 2, 'Eos sunt quia dicta vel earum.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(68, 17, 1, 2, 'Ut fugiat voluptatum rerum.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(69, 18, 1, 2, 'Non aut mollitia quam quos quis.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(70, 18, 1, 2, 'Illum architecto qui ipsa aperiam voluptatem cupiditate sed.', 1, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(71, 18, 1, 2, 'Aliquid aspernatur rerum voluptate inventore ut nam.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(72, 18, 1, 2, 'Magni iure aut cupiditate quasi quisquam.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(73, 19, 1, 2, 'Nulla aut molestias sit.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(74, 19, 1, 2, 'Ut a excepturi sint similique.', 1, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(75, 19, 1, 2, 'Ex rerum et.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(76, 19, 1, 2, 'Ex hic sint ad illum.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(77, 20, 1, 2, 'Excepturi corporis asperiores harum.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(78, 20, 1, 2, 'Consequatur excepturi dolor sit ducimus.', 1, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(79, 20, 1, 2, 'Esse blanditiis enim deleniti inventore quia.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(80, 20, 1, 2, 'Asperiores officiis molestias autem omnis aut.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(81, 21, 1, 3, 'Sit voluptatem aut quis maxime recusandae necessitatibus.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(82, 21, 1, 3, 'Quod est voluptatibus.', 1, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(83, 21, 1, 3, 'Qui nihil possimus voluptas qui qui saepe.', 0, '2020-09-12 05:53:25', '2020-09-12 05:53:25'),
(84, 21, 1, 3, 'Aperiam optio a.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(85, 22, 1, 3, 'Magni magnam sit commodi voluptatem.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(86, 22, 1, 3, 'Ut consectetur accusamus sunt mollitia autem.', 1, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(87, 22, 1, 3, 'Sed maxime quidem.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(88, 22, 1, 3, 'Nesciunt quia deserunt dolore in.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(89, 23, 1, 3, 'Harum quam quaerat doloribus velit facilis.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(90, 23, 1, 3, 'Porro assumenda sed qui enim voluptatem numquam laudantium.', 1, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(91, 23, 1, 3, 'Magni maiores id reprehenderit.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(92, 23, 1, 3, 'Harum quia officia distinctio possimus.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(93, 24, 1, 3, 'Magni eius enim nisi nesciunt.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(94, 24, 1, 3, 'Eveniet omnis in repellendus cum.', 1, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(95, 24, 1, 3, 'Mollitia aut dolores aliquid sint aut.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(96, 24, 1, 3, 'Fugiat voluptates ea quas dolor modi.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(97, 25, 1, 3, 'Omnis dignissimos cumque dignissimos id quia.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(98, 25, 1, 3, 'Similique repellendus eaque.', 1, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(99, 25, 1, 3, 'Sit itaque rem dolorem voluptate molestiae.', 0, '2020-09-12 05:53:26', '2020-09-12 05:53:26'),
(100, 25, 1, 3, 'Sed veniam neque nesciunt.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(101, 26, 1, 3, 'Accusamus dolorem quia dolorem et.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(102, 26, 1, 3, 'Delectus facere et officiis magni voluptas.', 1, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(103, 26, 1, 3, 'Voluptatem occaecati in et ducimus.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(104, 26, 1, 3, 'Ratione velit minus temporibus atque dolores.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(105, 27, 1, 3, 'Ullam qui eos nulla.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(106, 27, 1, 3, 'Voluptas quisquam totam nihil aliquam.', 1, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(107, 27, 1, 3, 'Consectetur inventore ex numquam officia.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(108, 27, 1, 3, 'Sit ea quibusdam ea aspernatur perferendis.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(109, 28, 1, 3, 'Iste mollitia qui quidem.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(110, 28, 1, 3, 'Nesciunt dignissimos repudiandae quibusdam assumenda nemo est.', 1, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(111, 28, 1, 3, 'Magnam enim unde ullam est facilis omnis.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(112, 28, 1, 3, 'Voluptas in placeat.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(113, 29, 1, 3, 'Sapiente iste maxime.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(114, 29, 1, 3, 'Odio autem itaque ex quis.', 1, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(115, 29, 1, 3, 'Qui et culpa enim.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(116, 29, 1, 3, 'Et porro reprehenderit officiis aut autem ratione.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(117, 30, 1, 3, 'Sit et delectus ex non.', 0, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(118, 30, 1, 3, 'Cumque architecto recusandae reiciendis.', 1, '2020-09-12 05:53:27', '2020-09-12 05:53:27'),
(119, 30, 1, 3, 'Omnis laudantium sint omnis.', 0, '2020-09-12 05:53:28', '2020-09-12 05:53:28'),
(120, 30, 1, 3, 'Qui explicabo sit eligendi eaque non quas.', 0, '2020-09-12 05:53:28', '2020-09-12 05:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `practise_tests`
--

CREATE TABLE `practise_tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `test_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `practise_tests`
--

INSERT INTO `practise_tests` (`id`, `user_id`, `test_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '2020-10-02 14:48:02', '2020-10-02 14:48:02'),
(2, 2, 3, '2020-10-02 16:32:59', '2020-10-02 16:32:59'),
(3, 2, 5, '2020-10-05 06:54:50', '2020-10-05 06:54:50'),
(4, 2, 6, '2020-10-05 06:55:02', '2020-10-05 06:55:02'),
(5, 2, 7, '2020-10-05 06:55:15', '2020-10-05 06:55:15'),
(6, 2, 8, '2020-10-05 06:57:31', '2020-10-05 06:57:31'),
(7, 2, 9, '2020-10-05 07:00:41', '2020-10-05 07:00:41'),
(8, 2, 10, '2020-10-05 07:18:58', '2020-10-05 07:18:58'),
(9, 2, 11, '2020-10-05 07:20:26', '2020-10-05 07:20:26'),
(10, 2, 12, '2020-10-05 07:28:42', '2020-10-05 07:28:42'),
(11, 2, 13, '2020-10-05 07:31:38', '2020-10-05 07:31:38'),
(12, 2, 14, '2020-10-05 07:32:10', '2020-10-05 07:32:10'),
(13, 2, 15, '2020-10-05 07:33:36', '2020-10-05 07:33:36'),
(14, 2, 16, '2020-10-05 11:47:09', '2020-10-05 11:47:09'),
(15, 2, 17, '2020-10-05 11:47:31', '2020-10-05 11:47:31'),
(16, 2, 18, '2020-10-05 12:19:42', '2020-10-05 12:19:42'),
(17, 2, 19, '2020-10-05 13:12:45', '2020-10-05 13:12:45'),
(18, 2, 20, '2020-10-05 13:12:58', '2020-10-05 13:12:58'),
(19, 2, 21, '2020-10-05 13:13:59', '2020-10-05 13:13:59'),
(20, 2, 22, '2020-10-05 13:19:01', '2020-10-05 13:19:01'),
(21, 2, 23, '2020-10-05 13:25:27', '2020-10-05 13:25:27'),
(22, 2, 24, '2020-10-05 13:26:04', '2020-10-05 13:26:04'),
(23, 2, 25, '2020-10-05 13:31:48', '2020-10-05 13:31:48'),
(24, 2, 26, '2020-10-05 13:40:32', '2020-10-05 13:40:32'),
(25, 2, 27, '2020-10-05 13:49:00', '2020-10-05 13:49:00'),
(26, 2, 28, '2020-10-05 14:02:46', '2020-10-05 14:02:46'),
(27, 2, 29, '2020-10-05 14:11:16', '2020-10-05 14:11:16'),
(28, 2, 30, '2020-10-05 14:11:30', '2020-10-05 14:11:30'),
(29, 2, 31, '2020-10-05 14:19:33', '2020-10-05 14:19:33'),
(30, 2, 32, '2020-10-05 14:21:17', '2020-10-05 14:21:17'),
(31, 2, 33, '2020-10-05 15:23:02', '2020-10-05 15:23:02'),
(32, 2, 34, '2020-10-05 15:37:57', '2020-10-05 15:37:57'),
(33, 2, 35, '2020-10-05 15:43:30', '2020-10-05 15:43:30'),
(34, 2, 36, '2020-10-05 15:52:12', '2020-10-05 15:52:12'),
(35, 2, 37, '2020-10-05 16:00:25', '2020-10-05 16:00:25'),
(36, 2, 38, '2020-10-05 16:02:45', '2020-10-05 16:02:45'),
(37, 2, 39, '2020-10-05 16:42:49', '2020-10-05 16:42:49');

-- --------------------------------------------------------

--
-- Table structure for table `practise_test_results`
--

CREATE TABLE `practise_test_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `practise_test_id` bigint(20) UNSIGNED NOT NULL,
  `marks_obtained` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `practise_test_results`
--

INSERT INTO `practise_test_results` (`id`, `practise_test_id`, `marks_obtained`, `created_at`, `updated_at`) VALUES
(1, 2, 13, '2020-10-02 16:34:36', '2020-10-02 16:34:36'),
(2, 4, 0, '2020-10-05 06:55:04', '2020-10-05 06:55:04'),
(3, 5, 4, '2020-10-05 06:55:26', '2020-10-05 06:55:26'),
(4, 6, 2, '2020-10-05 06:57:33', '2020-10-05 06:57:33'),
(5, 7, 4, '2020-10-05 07:00:50', '2020-10-05 07:00:50'),
(6, 8, 2, '2020-10-05 07:18:59', '2020-10-05 07:18:59'),
(7, 9, 5, '2020-10-05 07:20:48', '2020-10-05 07:20:48'),
(8, 10, 4, '2020-10-05 07:28:59', '2020-10-05 07:28:59'),
(9, 11, 2, '2020-10-05 07:31:40', '2020-10-05 07:31:40'),
(10, 13, 3, '2020-10-05 07:34:18', '2020-10-05 07:34:18'),
(11, 14, 0, '2020-10-05 11:47:11', '2020-10-05 11:47:11'),
(12, 15, 3, '2020-10-05 11:47:59', '2020-10-05 11:47:59'),
(13, 16, 4, '2020-10-05 12:19:58', '2020-10-05 12:19:58'),
(14, 17, 2, '2020-10-05 13:12:47', '2020-10-05 13:12:47'),
(15, 18, 2, '2020-10-05 13:13:12', '2020-10-05 13:13:12'),
(16, 19, 4, '2020-10-05 13:14:45', '2020-10-05 13:14:45'),
(17, 20, 1, '2020-10-05 13:19:29', '2020-10-05 13:19:29'),
(18, 21, 4, '2020-10-05 13:25:28', '2020-10-05 13:25:28'),
(19, 22, 5, '2020-10-05 13:26:26', '2020-10-05 13:26:26'),
(20, 23, 5, '2020-10-05 13:31:57', '2020-10-05 13:31:57'),
(21, 24, 4, '2020-10-05 13:41:51', '2020-10-05 13:41:51'),
(22, 25, 2, '2020-10-05 13:49:21', '2020-10-05 13:49:21'),
(23, 26, 5, '2020-10-05 14:02:58', '2020-10-05 14:02:58'),
(24, 27, 4, '2020-10-05 14:11:17', '2020-10-05 14:11:17'),
(25, 28, 4, '2020-10-05 14:11:48', '2020-10-05 14:11:48'),
(26, 29, 2, '2020-10-05 14:19:52', '2020-10-05 14:19:52'),
(27, 30, 3, '2020-10-05 14:21:26', '2020-10-05 14:21:26'),
(28, 31, 5, '2020-10-05 15:23:14', '2020-10-05 15:23:14'),
(29, 32, 4, '2020-10-05 15:38:16', '2020-10-05 15:38:16'),
(30, 33, 4, '2020-10-05 15:43:59', '2020-10-05 15:43:59'),
(31, 35, 2, '2020-10-05 16:02:01', '2020-10-05 16:02:01'),
(32, 36, 2, '2020-10-05 16:02:59', '2020-10-05 16:02:59'),
(33, 37, 2, '2020-10-05 16:42:59', '2020-10-05 16:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `chapter_id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` bigint(20) UNSIGNED NOT NULL,
  `difficulty_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'medium',
  `marks` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `chapter_id`, `subject_id`, `difficulty_level`, `marks`, `created_at`, `updated_at`) VALUES
(1, 'Updated question 3 time !', 1, 1, 'hard', 2, '2020-09-12 05:53:18', '2020-10-03 10:36:39'),
(2, 'Qui tempore eaque dolorum nostrum aliquid.', 1, 1, 'medium', 2, '2020-09-12 05:53:18', '2020-09-12 05:53:18'),
(3, 'Vitae voluptatem modi deleniti sunt deserunt.', 1, 1, 'medium', 2, '2020-09-12 05:53:18', '2020-09-12 05:53:18'),
(4, 'Officiis et quis explicabo neque est est ut impedit.', 1, 1, 'medium', 2, '2020-09-12 05:53:18', '2020-09-12 05:53:18'),
(5, 'Aut molestias voluptas eveniet ullam doloribus.', 1, 1, 'medium', 1, '2020-09-12 05:53:18', '2020-09-12 05:53:18'),
(6, 'Dolorem illo fugiat dolor nihil.', 1, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(7, 'Incidunt ut eveniet nesciunt et laboriosam et maxime perferendis eum.', 1, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(8, 'Quisquam quia dicta voluptatibus vero dolore incidunt soluta.', 1, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(9, 'Aut excepturi ipsum consequatur.', 1, 1, 'medium', 1, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(10, 'Odit minima quod quisquam.', 1, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(11, 'Qui ratione consequatur excepturi accusantium iure fuga.', 2, 1, 'medium', 1, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(12, 'Sint id maiores voluptatem saepe quo.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(13, 'Hic ipsam voluptates corporis iure.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(14, 'Error reprehenderit ut iusto.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(15, 'Reiciendis illum maiores qui.', 2, 1, 'medium', 1, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(16, 'Autem nesciunt sint esse corrupti velit dolorem aut maiores odio.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(17, 'Praesentium itaque voluptatibus perspiciatis.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(18, 'Voluptatem magni esse.', 2, 1, 'medium', 2, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(19, 'Officia rerum cum sunt sapiente.', 2, 1, 'medium', 1, '2020-09-12 05:53:19', '2020-09-12 05:53:19'),
(20, 'Voluptatem aliquid delectus rerum exercitationem earum.', 2, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(21, 'Voluptas hic et quibusdam.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(22, 'Debitis in facere qui ipsum doloribus ducimus.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(23, 'Velit voluptas asperiores in.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(24, 'Ratione ipsum molestiae deserunt est ut.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(25, 'Dolor odit rerum enim ipsam id nihil.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(26, 'Consectetur ut impedit occaecati quia ab.', 3, 1, 'medium', 1, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(27, 'Autem omnis qui reiciendis laboriosam veniam.', 3, 1, 'medium', 1, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(28, 'Repellat dolore molestias temporibus nisi.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(29, 'Velit illo molestias sequi.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20'),
(30, 'Et accusamus ex voluptate quibusdam id similique possimus.', 3, 1, 'medium', 2, '2020-09-12 05:53:20', '2020-09-12 05:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_tests`
--

CREATE TABLE `scheduled_tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `test_id` bigint(20) UNSIGNED NOT NULL,
  `scheduled_time` datetime NOT NULL,
  `chapter_info` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scheduled_tests`
--

INSERT INTO `scheduled_tests` (`id`, `user_id`, `test_id`, `scheduled_time`, `chapter_info`, `flag`, `created_at`, `updated_at`) VALUES
(1, 4, 1, '2020-10-01 03:30:00', '1-5,2-5', 0, '2020-10-01 19:11:09', '2020-10-01 19:11:09'),
(2, 4, 4, '2024-09-30 06:52:00', '1-5,2-5', 0, '2020-10-02 16:38:45', '2020-10-02 16:38:45'),
(3, 4, 40, '2024-09-30 19:07:00', '7-10', 0, '2024-10-01 14:36:54', '2024-10-01 14:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_test_results`
--

CREATE TABLE `scheduled_test_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `scheduled_tests_id` bigint(20) UNSIGNED NOT NULL,
  `marks_obtained` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scheduled_test_results`
--

INSERT INTO `scheduled_test_results` (`id`, `user_id`, `scheduled_tests_id`, `marks_obtained`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 7, '2020-10-01 19:19:39', '2020-10-01 19:19:39'),
(2, 2, 2, 4, '2020-10-02 16:53:43', '2020-10-02 16:53:43'),
(3, 2, 1, 0, '2024-10-01 14:38:20', '2024-10-01 14:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Software Engineering', '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(2, 'Microprocessor', '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(3, 'Java', '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(4, 'C Programming', '2020-10-01 19:09:51', '2020-10-01 19:09:51');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` bigint(20) UNSIGNED NOT NULL,
  `chapter_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_marks` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `subject_id`, `chapter_id`, `total_marks`, `created_at`, `updated_at`) VALUES
(1, 1, '1,2', 10, '2020-10-01 19:11:09', '2020-10-01 19:11:09'),
(2, 1, '1,2,3', 30, '2020-10-02 14:48:01', '2020-10-02 14:48:01'),
(3, 1, '1,2,3', 20, '2020-10-02 16:32:59', '2020-10-02 16:32:59'),
(4, 1, '1,2', 10, '2020-10-02 16:38:45', '2020-10-02 16:38:45'),
(5, 1, '1', 5, '2020-10-05 06:54:50', '2020-10-05 06:54:50'),
(6, 1, '1', 5, '2020-10-05 06:55:02', '2020-10-05 06:55:02'),
(7, 1, '1', 5, '2020-10-05 06:55:14', '2020-10-05 06:55:14'),
(8, 1, '1', 5, '2020-10-05 06:57:31', '2020-10-05 06:57:31'),
(9, 1, '1', 5, '2020-10-05 07:00:41', '2020-10-05 07:00:41'),
(10, 1, '1', 5, '2020-10-05 07:18:58', '2020-10-05 07:18:58'),
(11, 1, '1', 5, '2020-10-05 07:20:26', '2020-10-05 07:20:26'),
(12, 1, '1', 5, '2020-10-05 07:28:42', '2020-10-05 07:28:42'),
(13, 1, '1', 5, '2020-10-05 07:31:38', '2020-10-05 07:31:38'),
(14, 1, '1', 5, '2020-10-05 07:32:10', '2020-10-05 07:32:10'),
(15, 1, '1', 5, '2020-10-05 07:33:36', '2020-10-05 07:33:36'),
(16, 1, '1', 5, '2020-10-05 11:47:09', '2020-10-05 11:47:09'),
(17, 1, '1', 5, '2020-10-05 11:47:31', '2020-10-05 11:47:31'),
(18, 1, '1', 5, '2020-10-05 12:19:42', '2020-10-05 12:19:42'),
(19, 1, '1', 5, '2020-10-05 13:12:45', '2020-10-05 13:12:45'),
(20, 1, '1', 5, '2020-10-05 13:12:58', '2020-10-05 13:12:58'),
(21, 1, '1', 5, '2020-10-05 13:13:59', '2020-10-05 13:13:59'),
(22, 1, '2', 5, '2020-10-05 13:19:01', '2020-10-05 13:19:01'),
(23, 1, '1', 5, '2020-10-05 13:25:27', '2020-10-05 13:25:27'),
(24, 1, '1', 5, '2020-10-05 13:26:04', '2020-10-05 13:26:04'),
(25, 1, '1', 5, '2020-10-05 13:31:48', '2020-10-05 13:31:48'),
(26, 1, '1', 5, '2020-10-05 13:40:31', '2020-10-05 13:40:31'),
(27, 1, '1', 5, '2020-10-05 13:49:00', '2020-10-05 13:49:00'),
(28, 1, '1', 5, '2020-10-05 14:02:46', '2020-10-05 14:02:46'),
(29, 1, '1', 5, '2020-10-05 14:11:15', '2020-10-05 14:11:15'),
(30, 1, '1', 5, '2020-10-05 14:11:30', '2020-10-05 14:11:30'),
(31, 1, '1', 5, '2020-10-05 14:19:33', '2020-10-05 14:19:33'),
(32, 1, '1', 5, '2020-10-05 14:21:17', '2020-10-05 14:21:17'),
(33, 1, '1', 5, '2020-10-05 15:23:02', '2020-10-05 15:23:02'),
(34, 1, '1', 5, '2020-10-05 15:37:57', '2020-10-05 15:37:57'),
(35, 1, '1', 5, '2020-10-05 15:43:30', '2020-10-05 15:43:30'),
(36, 1, '2', 5, '2020-10-05 15:52:11', '2020-10-05 15:52:11'),
(37, 1, '3', 5, '2020-10-05 16:00:25', '2020-10-05 16:00:25'),
(38, 1, '2', 5, '2020-10-05 16:02:45', '2020-10-05 16:02:45'),
(39, 1, '2', 5, '2020-10-05 16:42:49', '2020-10-05 16:42:49'),
(40, 3, '7', 10, '2024-10-01 14:36:54', '2024-10-01 14:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jhon Doe', 'jhon@gmail.com', NULL, '$2y$10$57yRfgF/QDqKvTC9nrq.GeMMfSY4xtC9zEsoClfjXA995y6InOoFG', 'admin', NULL, '2020-10-01 19:09:49', '2020-10-01 19:09:49'),
(2, 'Yash Thakkar', 'yashthakkar2700@gmail.com', NULL, '$2y$10$57yRfgF/QDqKvTC9nrq.GeMMfSY4xtC9zEsoClfjXA995y6InOoFG', 'student', NULL, '2020-10-01 19:09:49', '2020-10-01 19:09:49'),
(3, 'Raj Panchal', 'raj@gmail.com', NULL, '$2y$10$QyAsXawIJXARr5Dd/B8Lo.6OuznHUuCNnc/ce37S8txGtLgl3OA4W', 'student', NULL, '2020-10-01 19:09:49', '2020-10-01 19:09:49'),
(4, 'Nick', 'nick@gmail.com', NULL, '$2y$10$57yRfgF/QDqKvTC9nrq.GeMMfSY4xtC9zEsoClfjXA995y6InOoFG', 'teacher', NULL, '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(5, 'Jimmy Doe', 'jimmy@gmail.com', NULL, '$2y$10$394fgDt1nBkaHOxyM8SOGe7VUt9RIN4v4erCsmTdTSvcTtlwMQ9ZK', 'student', NULL, '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(6, 'Jane Doe', 'jane@gmail.com', NULL, '$2y$10$e4Y.mE23FwbrDAH4y.Uk9ef/XH/iUApxgdzDCELc0Gj/NYhQ49/qS', 'student', NULL, '2020-10-01 19:09:50', '2020-10-01 19:09:50'),
(7, 'neo', 'neo@gmail.com', NULL, '$2y$10$57yRfgF/QDqKvTC9nrq.GeMMfSY4xtC9zEsoClfjXA995y6InOoFG', 'student', NULL, '2024-10-01 14:09:20', '2024-10-01 14:09:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chapters_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_chapter_id_foreign` (`chapter_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `practise_tests`
--
ALTER TABLE `practise_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `practise_test_results`
--
ALTER TABLE `practise_test_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_chapter_id_foreign` (`chapter_id`);

--
-- Indexes for table `scheduled_tests`
--
ALTER TABLE `scheduled_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheduled_test_results`
--
ALTER TABLE `scheduled_test_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subjects_name_unique` (`name`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `practise_tests`
--
ALTER TABLE `practise_tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `practise_test_results`
--
ALTER TABLE `practise_test_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `scheduled_tests`
--
ALTER TABLE `scheduled_tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scheduled_test_results`
--
ALTER TABLE `scheduled_test_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chapters`
--
ALTER TABLE `chapters`
  ADD CONSTRAINT `chapters_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_chapter_id_foreign` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_chapter_id_foreign` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
