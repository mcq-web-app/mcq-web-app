<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledTestResult extends Model
{
    protected $fillable = [
        'user_id',
        'scheduled_tests_id',
        'marks_obtained',
        'created_at',
        'updated_at'
    ];
}
