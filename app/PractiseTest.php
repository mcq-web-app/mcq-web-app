<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PractiseTest extends Model
{
    protected $fillable = [
        'user_id',
        'test_id',
        'created_at',
        'updated_at'
    ];
}
