<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    protected $fillable = [
        'question',
        'subject_id',
        'chapter_id',
        'difficulty_level',
        'marks'
    ];

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
