<?php

namespace App\Http\Controllers;
session_start();

use App\Option;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SolutionController extends Controller
{
    public function index(Request $request)
    {
        // dd(Session::has("9"."_question"));
        $all_questions = unserialize($request->all_questions_array);
        // dd($all_questions);
        $not_attempted = array();
        $attempted_questions = array();
        $right = array();
        $wrong = array();

        for($i = 0; $i < sizeof($all_questions); $i++)
        {
            $val = $all_questions[$i];
            
            if(Session::has($val."_question"))
            {
                
                // echo "session = ". Session::get($val."_question")."\n";
                $question_info = Question::where('id', $val)->get()[0];
                // echo($question_info);
                $subject_id = $question_info->subject_id;
                
                $option_info = Option::where([
                    'question_id'=>$val,
                    'correct'=>1
                ])->get()[0];
                // echo Session::get($val."_question") ."->".  $option_info->id. " val: ".$val."\n";
                if((Session::get($val."_question")) == $option_info->id)
                {
                    //right
                    // echo "right : ".$val."\n";
                    $right[$val] = $option_info->id;

                    //Unset the session
                    unset($_SESSION[$val."_question"]);
                }
                else
                {
                    //wrong
                    // echo "wrong : ".$val."\n";
                    $wrong[$val] = $option_info->id;

                    //Unset the session
                    unset($_SESSION[$val."_question"]);
                }

                array_push($attempted_questions, $val);
               
            }   
        }
        
        
        //Getting all NOT ATTEMPTED QUESTIONS
        for($z=0; $z < sizeof($all_questions); $z++)
        {
            if(in_array($all_questions[$z], $attempted_questions))
            {
                // nothing to do
            }
            else{
                array_push($not_attempted, $all_questions[$z]);
            }
        }

        // dd($not_attempted);

        return view('solution.index', compact([
            'right',
            'wrong',
            'attempted_questions',
            'not_attempted',
            'all_questions'
        ]));
    }
}
