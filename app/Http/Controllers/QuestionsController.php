<?php

namespace App\Http\Controllers;

use App\Question;
use App\Subject;
use App\Chapter;
use App\Option;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view('question.index', compact([
            'questions'
        ]));
    }

    public function index_specific(Request $request, Chapter $chapter, Subject $subject)
    {
        // dd($chapter);
        $questions = Question::where([
            'chapter_id'=>$chapter->id,
            'subject_id'=>$subject->id
        ])->get();
        // $chapter = $chapter;
        // $subject = $subject;
        return view('question.index', compact([
            'questions',
            'chapter',
            'subject'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Subject $subject, Chapter $chapter)
    {
        // $subject =  $request->subject;
        // $chapter = $request->chapter;
        return view('question.create', compact([
            'subject',
            'chapter'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd($request->optionRadios);
        $question = Question::create([
            'question'=>$request->question,
            'chapter_id'=>$request->chapters[0],
            'subject_id'=>$request->subjects[0],
            'marks'=>$request->marks,
            'difficulty_level'=>$request->difficulty_level[0]
        ]);

        // dd($question);

        $option1 = Option::create([
            'question_id'=>$question->id,
            'subject_id'=>$request->subjects[0],
            'chapter_id'=>$request->chapters[0],
            'option'=>$request->option1
        ]);
        $option2 = Option::create([
            'question_id'=>$question->id,
            'subject_id'=>$request->subjects[0],
            'chapter_id'=>$request->chapters[0],
            'option'=>$request->option2
        ]);
        $option3 = Option::create([
            'question_id'=>$question->id,
            'subject_id'=>$request->subjects[0],
            'chapter_id'=>$request->chapters[0],
            'option'=>$request->option3
        ]);
        $option4 = Option::create([
            'question_id'=>$question->id,
            'subject_id'=>$request->subjects[0],
            'chapter_id'=>$request->chapters[0],
            'option'=>$request->option4
        ]);

        if($request->optionRadios == 1)
        {
            Option::where('id',$option1->id)->update(['correct'=> 1]);
        }
        else if($request->optionRadios == 2)
        {
            Option::where('id',$option2->id)->update(['correct'=> 1]);
        }
        else if($request->optionRadios == 3)
        {
            Option::where('id',$option3->id)->update(['correct'=> 1]);
        }
        else if($request->optionRadios == 4)
        {
            Option::where('id',$option4->id)->update(['correct'=> 1]);
        }


        session()->flash('success', "Question Added Successfully");
        return redirect(route('subjects.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        // dd($question);
        $subject = Subject::where('id', $question->subject_id)->first();
        $chapter = Chapter::where('id', $question->chapter_id)->first();
        $options = Option::where('question_id', $question->id)->get();

        // dd($options);
        
        return view('question.edit', compact([
            'question',
            'subject',
            'chapter',
            'options'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $question->update([
            'question'=>$request->question,
            'marks'=> $request->marks,
            'difficulty_level'=> $request->difficulty_level[0]
        ]);

        $option1 = (int)$request->option1_id;
        $option2 =(int) $request->option2_id;
        $option3 = (int)$request->option3_id;
        $option4 = (int)$request->option4_id;

        if($request->optionRadios == 1)
        {
            Option::where('id',$option1)->update([
                'correct'=> 1,
                'option'=>$request->option1
            ]);

            Option::where('id',$option2)->update(['correct'=> 0]);
            Option::where('id',$option3)->update(['correct'=> 0]);
            Option::where('id',$option4)->update(['correct'=> 0]);
        }
        else if($request->optionRadios == 2)
        {
            Option::where('id',$option2)->update([
                'correct'=> 1,
                'option'=>$request->option2
            ]);
            Option::where('id',$option1)->update(['correct'=> 0]);
            Option::where('id',$option3)->update(['correct'=> 0]);
            Option::where('id',$option4)->update(['correct'=> 0]);
        }
        else if($request->optionRadios == 3)
        {
            Option::where('id',$option3)->update([
                'correct'=> 1,
                'option'=>$request->option3
            ]);
            Option::where('id',$option1)->update(['correct'=> 0]);
            Option::where('id',$option2)->update(['correct'=> 0]);
            Option::where('id',$option4)->update(['correct'=> 0]);
        }
        else if($request->optionRadios == 4)
        {
            Option::where('id',$option4)->update([
                'correct'=> 1,
                'option'=>$request->option4
            ]);
            Option::where('id',$option1)->update(['correct'=> 0]);
            Option::where('id',$option2)->update(['correct'=> 0]);
            Option::where('id',$option3)->update(['correct'=> 0]);
        }
        session()->flash('success', "Subject has been updated successfully!");
        return redirect(route('question.index_specific', [$question->chapter_id, $question->subject_id]));
    }

    public function destroy(Question $question)
    {
        //
    }

    public function getChapters(Request $request, $id)
    {
        // dd("got it !");
        $chapters = Chapter::where('subject_id', $id)->get();
        echo json_encode($chapters);
    }
}
