<?php

namespace App\Http\Controllers;

use App\ScheduledTest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->isAdmin())
            return view('admin.index');
        elseif(auth()->user()->isTeacher())
        {
            $all_scheduled_tests = ScheduledTest::all();
            return view('teacher.index', compact([
                'all_scheduled_tests'
            ]));
        }
        elseif(auth()->user()->isStudent())
        {
            $all_scheduled_tests = ScheduledTest::all();
            // dd("HOEe");
            return view('student.index', compact([
                'all_scheduled_tests'
            ]));
        }
            
        return view('home');
    }
}
