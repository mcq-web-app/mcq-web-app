<?php

namespace App\Http\Controllers;
session_start();

use App\Option;
use App\PractiseTest;
use App\PractiseTestResult;
use App\Question;
use App\ScheduledTest;
use App\ScheduledTestResult;
use App\ScheduleTestResult;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mockery\Undefined;

class ResultController extends Controller
{
    public function index(Request $request)
    {
        // dd(unserialize($request->all_questions_array));
        $subject_id = "";
        $total_test_marks = $request->total_test_marks;
        $total_marks = 0;
        $not_attempted = array();
        $all_questions = unserialize($request->all_questions_array);
     
        for($i = 0; $i < sizeof($all_questions); $i++)
        {
            $val = $all_questions[$i];
            // echo "val = ".$val."\n";
            
            if(Session::has($val."_question"))
            {
                // echo "session = ". Session::get($val."_question")."\n";
                $question_info = Question::where('id', $val)->get()[0];
                // echo($question_info);
                $subject_id = $question_info->subject_id;
                
                $option_info = Option::where([
                    'question_id'=>$val,
                    'correct'=>1
                ])->get()[0];
                
                if((Session::get($val."_question")) == $option_info->id)
                {
                  
                    $total_marks += $question_info->marks;

                    //Unset the session
                    // unset($_SESSION[$val."_question"]);
                }
            }   
        }
        //Inserting test result in practise_test_results table
        if($request->test_type == "practise")
        {
            PractiseTestResult::create([
                'practise_test_id'=> $request->practise_test_id,
                'marks_obtained'=> $total_marks
            ]);
        }
        else if($request->test_type == "scheduled")
        {
            ScheduledTestResult::create([
                'user_id'=>auth()->user()->id,
                'scheduled_tests_id'=> $request->schedule_test_id,
                'marks_obtained'=> $total_marks
            ]);
        }
        

        //Fetching subject name
        $subj_name = Subject::where('id', $subject_id)->get()[0]->name;
        
        return view('result.index', compact([
            'total_marks',
            'total_test_marks',
            'subj_name',
            'all_questions'
        ]));
    }

    public function clearSessionAndGoToHome(Request $request)
    {   
        // dd($request);
        $all_questions = unserialize($request->all_questions_array);
        // dd($all_questions);

        //Unsetting the session
        for($i = 0; $i < sizeof($all_questions); $i++)
        {
            $val = $all_questions[$i];
            // echo "val = ".$val."\n";
            
            if(Session::has($val."_question"))
            {
                unset($_SESSION[$val."_question"]);
            }   
        }

        $all_scheduled_tests = ScheduledTest::all();

        return view('student.index', compact([
            'all_scheduled_tests'
        ]));
    }
}
