<?php

namespace App\Http\Controllers;

use App\ScheduledTest;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == "student"){
            $all_scheduled_tests = ScheduledTest::all();
            
            return view('student.index', compact([
                'all_scheduled_tests'
            ]));
        }
        abort(403);
    }
}

