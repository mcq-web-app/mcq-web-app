<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Option;
use App\PractiseTest;
use App\Question;
use App\ScheduledTest;
use App\ScheduleTest;
use App\Test;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TestsController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == "student"){
            return view('test.index');
        }
        abort(403);
    }

    public function store(Request $request)
    {
        //storing into test table
        // dd($request);

        /* $chp_info = "1-10, 2-33, 3-56";
        $splitted = explode(",", $chp_info);
        
        for($k=0; $k<sizeof($splitted); $k++)
        {
            $split2 = explode("-", $splitted[$k]);

            $chapter_id = $split2[0];
            $chapter_marks = $split2[1];
            echo($chapter_id . "->" . $chapter_marks);
        }
        dd("----------------"); */
       
        $chp_with_marks = "";

        $chapters = Chapter::where('subject_id', $request->subject_id)->get();
        // dd($chapters);
        $chapterMarks = array();

        $chapter_str = "";
        $count = 0;
        foreach($chapters as $chapter)
        {
            $key = $chapter->id;
            if($request->$key)
            {
                $count += 1;
            }
        }
        $count -= 1;

        foreach($chapters as $chapter)
        {
            $key = $chapter->id;

            if($request->$key)
            {
                $chapterMarks[$key] = $request->$key;

                $chapter_str .= strval($chapter->id);

                $chp_with_marks .= $key . "-" . $chapterMarks[$key];

                if($count > 0)
                {
                    $chapter_str .=",";
                    $count -= 1;

                    $chp_with_marks .= ",";
                }

            }
        }

        $test = Test::create([
            'subject_id'=>$request->subject_id,
            'chapter_id'=>$chapter_str,
            'total_marks'=>$request->totalMarks
        ]);
        $duration = strtotime("00:00:00");
        if(auth()->user()->role == "teacher")
        {
            
            // $sdt = explode("T",$request->scheduleDate);
            // $scheduled_dateTime = $sdt[0] . " " . $sdt[1];
            $date = $request->scheduleDate;

            // dd($request->$date);

            $schedule_test = ScheduledTest::create([
                'user_id'=> auth()->user()->id,
                'test_id'=> $test->id,
                'scheduled_time'=> $date,
                'chapter_info'=>$chp_with_marks,
                'flag'=>0 
            ]);

            // dd($schedule_test);
            $all_scheduled_tests = ScheduledTest::where('user_id', auth()->user()->id)->get();
            // dd($all_scheduled_tests);
            return view('teacher.index', compact([
                'all_scheduled_tests'
            ]));
        }
        else if(auth()->user()->role == "student")
        {
            $practise_test = PractiseTest::create([
                'user_id'=>auth()->user()->id,
                'test_id'=>$test->id
            ]);
        }

        // dd($practise_test);

        $total_marks =   (int)$request->totalMarks;
        // dd(var_dump($total_marks));

        // $questions_array = Test::startTest($test, $practise_test, $chapterMarks);
        $questions_array = Test::startTest($test, $chapterMarks);
       
        //Fetching first question and option
        $first_question = Question::where('id', $questions_array[0])->get()[0];
        $first_option = Option::where('question_id', $first_question->id )->get();

        $second_question = $questions_array[1];

        $test_type = "practise";

        return view('test.index', compact([
            'questions_array',
            'first_question',
            'first_option',
            'second_question',
            'total_marks',
            'practise_test',
            'test_type'
        ]));

    }

    public function schedulingTest(Request $request)
    {
        // dd($request->schedule_test_id);
        $test_type = "scheduled";
        $chapterMarks = array();
        $schedule_test = ScheduledTest::where('test_id', $request->schedule_test_id)->first();
        // dd($schedule_test);
        // dd($schedule_test);
        $test = Test::where('id', $schedule_test->test_id)->get()[0];
       
        $splitted = explode(",", $schedule_test->chapter_info);
        
        for($k=0; $k<sizeof($splitted); $k++)
        {
            $split2 = explode("-", $splitted[$k]);
           
            $chapter_id = $split2[0];
            $chapter_marks = $split2[1];
            $chapterMarks[$chapter_id] = $chapter_marks;
            echo($chapter_id . "->" . $chapter_marks);
        }
        //TODO - now do Test::startTest($test, $chapterMarks);
        
        $questions_array = Test::startTest($test, $chapterMarks);
        
        //Fetching first question and option
        $first_question = Question::where('id', $questions_array[0])->get()[0];
        $first_option = Option::where('question_id', $first_question->id )->get();
        $second_question = $questions_array[1];
        $total_marks = $test->total_marks;

        return view('test.index', compact([
            'questions_array',
            'first_question',
            'first_option',
            'second_question',
            'total_marks',
            'schedule_test',
            'test_type'
        ]));

    }

    public function fetchQuestions(Request $request)
    {
        // dd($request->currentQuestionID ."  ->  ". $request->selectedOptionId);
        // STORING INTO SESSION
        Session::put($request->currentQuestionID."_question", $request->selectedOptionId);

        // dd("got ID: ". Session::get($request->currentQuestionID));

        $question = Question::where('id',$request->name)->get()[0];
        $option = Option::where('question_id', $request->name)->get();

        $option1 = $option[0];
        $option2 = $option[1];
        $option3 = $option[2];
        $option4 = $option[3];
        // dd($question);

        $info = [$question, $option1, $option2, $option3, $option4 ];
        // echo json_encode($question);
        echo json_encode($info);
    }
}
