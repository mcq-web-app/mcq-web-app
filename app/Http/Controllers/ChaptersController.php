<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Subject;
use Illuminate\Http\Request;

class ChaptersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Subject $subject)
    {
        // dd($subject->id);
        $chapters = Chapter::where('subject_id', $subject->id)->get();
        $subject_id = $subject->id;

        return view('chapter.index', compact([
            'chapters',
            'subject_id'
        ]));
    }

    public function manage()
    {
        $chapters = Chapter::all();
        return view('chapter.manage', compact([
            'chapters'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Subject $subject)
    {
        // dd($subject);
        // $subject = Subject::all();
        return view('chapter.create', compact([
            'subject'
        ]));

        session()->flash('success', "Chapter Added Successfully");
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->subjects[0]);
        Chapter::create([
            'name'=> $request->name,
            'subject_id'=> $request->subjects[0]
        ]);
        session()->flash('success', "Chapter Added Successfully");
        return redirect(route('subjects.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter)
    {
        // dd($chapter);
        $chapter = Chapter::where('id', $chapter->id)->get()[0];
        // dd($chapter);

        return view('chapter.edit', compact([
            'chapter'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapter $chapter)
    {
        // dd($chapter);
        $chapter->update([
            'name'=>$request->name
        ]);
        session()->flash('success', "Chapter has been updated successfully!");
        return redirect(route('subjects.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter)
    {
        //
    }
}
