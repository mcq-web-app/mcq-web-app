<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Subject;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return view('subject.index', compact([
            'subjects'
        ]));
    }

    public function manage()
    {
        $subjects = Subject::all();
        return view('subject.manage', compact([
            'subjects'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Subject::create([
            'name'=>$request->name
        ]);

        session()->flash('success', "Subject Added Successfully");
        return redirect(route('subjects.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        $subject = Subject::where('id', $subject->id)->get()[0];
        $chapters = Chapter::where('subject_id', $subject->id)->get();
        return view('subject.show', compact([
            'subject',
            'chapters'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {

        $subject = Subject::where('id', $subject->id)->get()[0];
        // dd($subject);

        return view('subject.edit', compact([
            'subject'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        // dd($subject);

        $subject->update([
            'name'=>$request->name
        ]);
        session()->flash('success', "Subject has been updated successfully!");
        return redirect(route('subjects.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        //
    }
}
