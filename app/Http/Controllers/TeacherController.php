<?php

namespace App\Http\Controllers;

use App\ScheduledTest;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == "teacher"){
            $all_scheduled_tests = ScheduledTest::all();
            return view('teacher.index', compact([
                'all_scheduled_tests'
            ]));
        }
        abort(403);
    }
}
