<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Test extends Model
{
    protected $fillable = [
        'subject_id', 
        'chapter_id',
        'total_marks'
    ];

    public static function checkGenerationCapability( $questions, $marks)
    {
        $current_m = 0;
        for($i=0; $i < sizeof($questions); $i++)
        {
            $current_m += $questions[$i]->marks;
        } 
        if($current_m < $marks)
        {
            
            return false;
        }

        return true;
    }
    

    // public static function startTest( Test $test, PractiseTest $practise_test, $chapterWithMarks)
    public static function startTest( Test $test, $chapterWithMarks)
    {
        // dd($chapterWithMarks);
        $chapterIds = $test->chapter_id;

        $fetched = explode(',',$chapterIds);
        // dd($fetched);

        $questions = Question::where([
            'subject_id'=> $test->subject_id
        ])->get();

        // dd($questions);

        $current_marks = 0;
        $total_marks = 0;
        $questions_array = array();
        $red_flag = 0;
        for($i = 0; $i < sizeof($fetched); $i++)
        {
            $marks = $chapterWithMarks[$fetched[$i]];

            $current_marks = 0;

            $questionsOfChapter = Question::where([
                'subject_id'=> $test->subject_id,
                'chapter_id'=>$fetched[$i]
            ])->get();


            //CHECKING that we have that much of questions in database
            if(!(Test::checkGenerationCapability($questionsOfChapter, $marks)))
            {
                $red_flag = 1;
                break;
            }
            
            // dd($questionsOfChapter);

            if($red_flag != 1)
            {

                while(( $current_marks < $marks ))// && $total_marks <= $test->total_marks)
                {
                    $random_index = rand(0, sizeof($questionsOfChapter)-1);

                    $id = $questionsOfChapter[$random_index]->id;
                    if(!in_array($id, $questions_array)) //if id not present then run if loop
                    {
                        if($questionsOfChapter[$random_index]->chapter_id == $fetched[$i])
                        {
                            array_push($questions_array, $id);
                            $current_marks += $questionsOfChapter[$random_index]->marks;
                            $total_marks += $current_marks;
                        }
                    }

                    if($current_marks > $marks)// || $total_marks > $test->total_marks)
                    {
                        $last_question_id = array_pop($questions_array);
                        $unwantedQuestion = Question::where([
                            'id'=> $last_question_id
                        ])->get();

                        // dd($current_marks);
                        $current_marks -= $unwantedQuestion[0]->marks;
                        $total_marks -= $unwantedQuestion[0]->marks;
                    }
                }  
            }else{
                    dd("catched by else! SOME ERROR... PLEASE TRY AFTER SOME TIME :)");      
            }
        }

        if($red_flag == 1)
        {
            dd("CAN'T GENERATE !!! We dont have that much questions .... SORRY! :)");
        }        
        

        // dd($questions_array);

        return $questions_array;
    }
}
