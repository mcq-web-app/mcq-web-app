<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'subject_id',
        'chapter_id',
        'question_id',
        'option',
        'correct'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

}
