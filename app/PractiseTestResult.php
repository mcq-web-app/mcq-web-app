<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PractiseTestResult extends Model
{
    protected $fillable = [
        'practise_test_id',
        'marks_obtained',
        'created_at',
        'updated_at'
    ];
}
