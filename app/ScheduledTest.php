<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledTest extends Model
{
    protected $fillable = [
        'user_id',
        'test_id',
        'scheduled_time',
        'chapter_info',
        'flag',
        'created_at',
        'updated_at'
    ];
}
