@extends('layouts.admin')
@section('title','MCQ-App | Admin')

@section('main-content')
  <div class="container">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
        <h1 class="h3 mb-4 text-gray-800">Admin Index</h1>
        <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list-ul fa-sm text-white"></i>
        </a>
    </div>

    <!-- <div class="card  border-primary mb-3" style="max-width: 26rem;">
        
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text">
                    <p>Microprocessor</p>
                    <form action="" method="POST">
                       
                        <button class="btn btn-link " type="submit">
                             All Chapters
                        </button>
                    </form>
                </p>
            </div>
        </div> -->

</div>
@endsection

@section('page-level-scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
@endsection






