@extends('layouts.app')
@section('title','MCQ-App | Teacher')

@section('content')

<div class="container">

    <!-- Page Heading -->
    <div class="heading">
        <h3>Teacher Index</h3>
        <strong>{{auth()->user()->name}}</strong>
            @foreach ($all_scheduled_tests as $tests)
                <p>Test id: {{ $tests->test_id }}</p>
            @endforeach
    </div>

@endsection

