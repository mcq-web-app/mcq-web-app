@extends('layouts.app')
@section('title','MCQ-App | Student')

@section('page-level-styles')
<link rel="stylesheet" href="{{asset('css/jquery.countdown.css')}}">
@endsection

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-12">

        <!-- FORM STARTED -->
        <form action="{{ route('result.index') }}" id="form" method="POST">
            @csrf
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>
                        <h6 class="font-weight-bold">Practise Test</h6>
                        <h4 class="">Software Engineering</h4>
                    </div>
                    <div>
                        <h6 class="font-weight-bold float-right">Time Left</h6>
                        <div id="countdown"></div>
                    </div>
                </div>
                   
                <div class="card-body" >   
                    <!-- SETTING TYPE OF TEST -->
                    <input type="hidden" name="test_type" value="{{$test_type}}">
                    <!-- SETTING TOTAL MARKS -->
                    <input type="hidden" name="total_test_marks" value="{{$total_marks}}">
                    <?php
                        if($test_type == "practise")
                        {
                    ?>
                            <!-- SETTING PRACTISE TEST ID -->
                            <input type="hidden" name="practise_test_id" value="{{ $practise_test->id }}">
                    <?php
                        }
                        else if($test_type == "scheduled")
                        {
                    ?>
                            <input type="hidden" name="schedule_test_id" value="{{ $schedule_test->id }}">
                    <?php
                        }
                    ?>
                    

                    <!-- ALL QUESTIONS ARRAY -->
                    <input type='hidden' name='all_questions_array' value='<?php echo htmlentities(serialize($questions_array)); ?>' />

                    <span class="question_number_text">Question 1</span>
                    <h5 id="question_{{ $first_question->id }}" class="card-title mt-3 mb-3 question" data-value="{{ $first_question->id }}"> {{ $first_question->question }} </h5>

                    <div class="form-group ml-4">
                        <div class="option">
                            <input class="form-check-input option_one"  name="optionRadios" type="radio" value="{{ $first_option[0]->id }}" id="{{ $first_option[0]->id }}">
                            <p class="ml-2 option1"> {{ $first_option[0]->option }} </p>
                        </div>
                        <div class="option">
                            <input class="form-check-input option_two"  name="optionRadios" type="radio" value="{{ $first_option[1]->id }}" id="{{ $first_option[1]->id }}">
                            <p class="ml-2 option2"> {{ $first_option[1]->option }} </p>
                        </div>
                        <div class="option">
                            <input class="form-check-input option_three"  name="optionRadios" type="radio" value="{{ $first_option[2]->id }}" id="{{ $first_option[2]->id }}">
                            <p class="ml-2 option3" > {{ $first_option[2]->option }} </p>
                        </div>
                        <div class="option">
                            <input class="form-check-input option_four" name="optionRadios"  type="radio" value="{{ $first_option[3]->id }}" id="{{ $first_option[3]->id }}">
                            <p class="ml-2 option4" > {{ $first_option[3]->option }} </p>
                        </div>
                    </div>

                    <input type="hidden" id="previous_input" value="">
                    <input type="hidden" id="save_input" value="{{ $second_question }}">
                    <input type="hidden" id="next_input" value="{{ $second_question }}">

                    <a href="#" class="btn btn-primary mt-4 mr-4" data-previous = "" id="previous">Previous</a>
                    <a href="#" class="btn btn-success mt-4 float-right saveBtn saved" data-save = "" id="save">Save</a>
                    <a href="#" class="btn btn-primary mt-4 mr-4 float-right" data-next = ""  id="next">Next</a>

                    
                    <input type="submit" class="btn btn-danger mt-4" value="Submit">
                </div>
            </div>

            </form>
            <!-- FORM ENDED -->
        </div>
    </div>

    <div class="row my-4">
        <div class="col-md-12 my-2">
            <div id="Question_number_outer">
                <div class="btn btn-link border-primary question-number mx-1 Question_number_btn active-question" id="{{ $questions_array[0] }}">
                    1
                </div>

                @for($i=1; $i < sizeof($questions_array); $i++) 
                
                    <div class="btn btn-link border-primary question-number mx-1 Question_number_btn" id="{{ $questions_array[$i] }}">
                        {{ $i + 1 }}
                    </div>
             
                @endfor
            </div>
        </div>
    </div>

</div>
@endsection



@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" integrity="sha512-3j3VU6WC5rPQB4Ld1jnLV7Kd5xr+cq9avvhwqzbH/taCRNURoeEpoPBK9pDyeukwSxwRPJ8fDgvYXd6SkaZ2TA==" crossorigin="anonymous"></script>
<script>

    var refresh = 0;
    
    
    if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
        console.info( "This page is reloaded" );
        
    } else {
        var ele = sessionStorage.getItem("refresh");
        if(ele == 1)
        {
            console.log("inside if  = "+refresh);
            sessionStorage.clear();
            document.getElementById('form').submit();
            
        }else{
            console.log("inside else  = "+refresh);
            refresh = 1;
            sessionStorage.setItem("refresh", refresh);
        }
    }
    
   
    // PAGE REFRESH EVENT
    window.onbeforeunload = function(e) {
        console.log("Just Refreshed!");
        // document.getElementById('form').submit();
        
    }

    // TAB CHANGE EVENT 
    document.addEventListener("visibilitychange", event => {
        if (document.visibilityState == "visible") {
            console.log("tab is active")
        } else {
            console.log("tab is inactive")
        }
    })

    // TIMER
    $(function(){

        var today = new Date(),
            totalMarks = <?php echo $total_marks; ?>;
            duration = 1;

        if(totalMarks <= 25){
            duration = 30;
            today.setMinutes(today.getMinutes() + duration);
        }
        else if(totalMarks <= 60){
            duration = 2;
            today.setHours(today.getHours() + duration);
        }
        else{
            duration = 3;
            today.setHours(today.getHours() + duration);
        }

        

        $('#countdown').countdown({
            timestamp	: today.getTime(),
        });

    });

    /*****************   jquery.countdown.js    ***********************/
      
    (function($){

// Number of seconds in every time division
var hours   = 60*60,
    minutes = 60;

// Creating the plugin
$.fn.countdown = function(prop){

    var options = $.extend({
        callback    : function(){},
        timestamp   : 0
    },prop);

    var left, h, m, s, positions;

    // Initialize the plugin
    init(this, options);

    positions = this.find('.position');

    (function tick(){

        // Time left
        left = Math.floor((options.timestamp - (new Date())) / 1000);

        if(left < 0){
            left = 0;
            console.log("done");
            sessionStorage.clear();
            document.getElementById('form').submit();
            return;
        }

        // Number of hours left
        h = Math.floor(left / hours);
        updateDuo(0, 1, h);
        left -= h*hours;

        // Number of minutes left
        m = Math.floor(left / minutes);
        updateDuo(2, 3, m);
        left -= m*minutes;

        // Number of seconds left
        s = left;
        updateDuo(4, 5, s);

        // Scheduling another call of this function in 1s
        setTimeout(tick, 1000);
    })();

    // This function updates two digit positions at once
    function updateDuo(minor,major,value){
        switchDigit(positions.eq(minor),Math.floor(value/10)%10);
        switchDigit(positions.eq(major),value%10);
    }

    return this;
};


function init(elem, options){
    elem.addClass('countdownHolder');

    // Creating the markup inside the container
    $.each(['Hours','Minutes','Seconds'],function(i){
        $('<span class="count'+this+'">').html(
            '<span class="position">\
                <span class="digit static">0</span>\
            </span>\
            <span class="position">\
                <span class="digit static">0</span>\
            </span>'
        ).appendTo(elem);

        if(this!="Seconds"){
            elem.append('<span class="countDiv countDiv'+i+'"></span>');
        }
    });

}

// Creates an animated transition between the two numbers
function switchDigit(position,number){

    var digit = position.find('.digit')

    if(digit.is(':animated')){
        return false;
    }

    if(position.data('digit') == number){
        // We are already showing this number
        return false;
    }

    position.data('digit', number);

    var replacement = $('<span>',{
        'class':'digit',
        css:{
            top:'-2.1em',
            opacity:0
        },
        html:number
    });

    // The .static class is added when the animation
    // completes. This makes it run smoother.

    digit
        .before(replacement)
        .removeClass('static')
        .animate({top:'2.5em',opacity:0},'fast',function(){
            digit.remove();
        })

    replacement
        .delay(100)
        .animate({top:0,opacity:1},'fast',function(){
            replacement.addClass('static');
        });
}
})(jQuery);

     /***************************************/

$('#form').submit(function(event) {

    event.preventDefault(); //this will prevent the default submit

    $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
})



function getNextId(questionId)
{
    var all_questions = "<?php echo json_encode($questions_array); ?>";
    
    // console.log(all_questions);
    var allQues = [];
    var counter = 0;
    var temp = "";
    for(var i=0; i < all_questions.length; i++)
    {
        if(all_questions[i] === '[' || all_questions[i] === ']' || all_questions[i] === ',' )
        { 
            allQues[counter] = temp;//all_questions[i];
            counter += 1;
            temp = "";
         }else{
            temp +=  all_questions[i];
            // console.log("got some!");
            
        }
    }

    // console.log("allQues = "+allQues);
    
    var nextQuestionId;
    for(var i=0; i < allQues.length; i++)
    {
        if(allQues[i] == questionId)
        {
            if(i == allQues.length-1){
                $(".question_number_text").html("Question "+(allQues.length-1));
                nextQuestionId = allQues[allQues.length-1]; //setting to last ele
                break;
            }
            else{
                $(".question_number_text").html("Question "+(i));
                nextQuestionId = allQues[i+1];
                // console.log("nextQUESTION ID = "+nextQuestionId);
                break;
            }
            // console.log("all_question[i] = "+all_questions[i]+", questionId = "+questionId);  
        }
    }
    return nextQuestionId;
}

function getPreviousId(questionId)
{
    var all_questions = "<?php echo json_encode($questions_array); ?>";
    
    console.log(all_questions);
    var allQues = [];
    var counter = 0;
    var temp = "";
    for(var i=0; i < all_questions.length; i++)
    {
        if(all_questions[i] === '[' || all_questions[i] === ']' || all_questions[i] === ',' )
        { 
            if(temp != ""){
                allQues[counter] = temp;
                counter += 1;
                temp = "";
            }
            
         }else{
            temp +=  all_questions[i];
        }
    }

    // console.log("allQues = "+allQues);
    
    var previousQuestionId;
    for(var i=0; i < allQues.length; i++)
    {
        if(allQues[i] == questionId)
        {
            if(i == 0){
                // $("#next").prop("disabled", true);
        
                $(".question_number_text").html("Question "+i);
                previousQuestionId = allQues[0]; //setting to first ele
                break;
            }
            else{
                if(i == 1)
                    $(".question_number_text").html("Question "+i);
                else
                    $(".question_number_text").html("Question "+(i-1));
                
                previousQuestionId = allQues[i-1];
                // console.log("previousQuestionId ID = "+previousQuestionId);
                break;
            }
            // console.log("all_question[i] = "+all_questions[i]+", questionId = "+questionId);  
        }
    }
    return previousQuestionId;
}


$("#Question_number_outer").on('click', '.Question_number_btn', function(){

    var curr_qId = $(".question").get(0).id;
    var curr_questionId = curr_qId.split("_")[1];
    var selectedOptionId = $('input[name=optionRadios]:checked', '#form').val();

    if(selectedOptionId != undefined)
    {
        console.log("INSIDE IF: "+selectedOptionId);
        // DO NOTHING as the id is set in selectedOptionId
    }else{
        console.log("else : "+selectedOptionId);
        selectedOptionId  = -1;
    }
    
    var question = this.id;
    var question_number = this.innerHTML;

    // Getting next Question ID
    var nextId = getNextId(question);
    // console.log("nextId = "+nextId);

    $("#save_input").val(nextId);
    $("#next_input").val(nextId);


    let route = "{{ route('test.fetchQuestions')}}";
    $.ajax({
        
        url: route,
        method: 'POST',
        dataType: 'json',
        data:{
            _token: "{{ csrf_token() }}",
            'name': question,
            'currentQuestionID': curr_questionId,
            'selectedOptionId': selectedOptionId
        },
        success: function(info){
            
            $(".question").attr('id',"question_"+info[0].id);
            $(".option_one").val(info[1].id);
            $(".option_two").val(info[2].id);
            $(".option_three").val(info[3].id);
            $(".option_four").val(info[4].id);

            $(".option_one").attr('id', info[1].id);
            $(".option_two").attr('id', info[2].id);
            $(".option_three").attr('id', info[3].id);
            $(".option_four").attr('id', info[4].id);            

            $(".Question_number_btn").removeClass("active-question");

            $('#'+info[0].id).addClass("active-question");
            
            $(".question_number_text").html("Question "+question_number);

            $(".question").html(info[0].question);

            $(".option1").html(info[1].option);
            $(".option2").html(info[2].option);
            $(".option3").html(info[3].option);
            $(".option4").html(info[4].option);
                         

            // setting option to empty or pre-filled
            if(sessionStorage.getItem(info[0].id)){
                // console.log(sessionStorage.getItem(info[0].id));
                var tp = sessionStorage.getItem(info[0].id);
                $('#'+tp).prop("checked", true); 
            }
            else{
                $(".option_one").prop("checked", false);
                $(".option_two").prop("checked", false);
                $(".option_three").prop("checked", false);
                $(".option_four").prop("checked", false);
            }
        }
    })
});

$('#form input').on('change', function() {
//    alert($('input[name=optionRadios]:checked', '#form').val()); 
});

$("#form").on('click', '#save , #next', function(){

    var curr_qId = $(".question").get(0).id;
    var curr_questionId = curr_qId.split("_")[1];
    var selectedOptionId = $('input[name=optionRadios]:checked', '#form').val();

    if(selectedOptionId != undefined)
    {
        // DO NOTHING as the id is set in selectedOptionId
        console.log("INSIDE IF: "+selectedOptionId);
    }else{
        console.log("else : "+selectedOptionId);
        selectedOptionId  = -1;
    }

    // sessionStorage.setItem("myVar", "Hello World!");
    // console.log(sessionStorage.getItem("myVar"));

    var qId = $(".question").get(0).id;
    var questionId = qId.split("_")[1];
     
    var selectedOptionId = $('input[name=optionRadios]:checked', '#form').val();

    if(selectedOptionId != undefined)
    {
        // Storing into the SESSION if any of the option is checked 
        sessionStorage.setItem(questionId, selectedOptionId);
    }
    
    
    
    var nextId = $("#save_input").val();
    console.log("nextId = "+nextId);

    var next = getNextId(nextId);
    $("#save_input").val(next);
    $("#next_input").val(next);
    // var nextNum = $("#save").data('nextnum');

    let route = "{{ route('test.fetchQuestions')}}";
    $.ajax({
        
        url: route,
        method: 'POST',
        dataType: 'json',
        data:{
            _token: "{{ csrf_token() }}",
            'name': nextId,
            'currentQuestionID': curr_questionId,
            'selectedOptionId': selectedOptionId
        },
        success: function(info){
            
            // $("#ques").attr('data-value',info[0].id);
            $(".question").attr('id', "question_"+info[0].id);
            $(".option_one").val(info[1].id);
            $(".option_two").val(info[2].id);
            $(".option_three").val(info[3].id);
            $(".option_four").val(info[4].id);

            $(".option_one").attr('id', info[1].id);
            $(".option_two").attr('id', info[2].id);
            $(".option_three").attr('id', info[3].id);
            $(".option_four").attr('id', info[4].id);            

            $(".Question_number_btn").removeClass("active-question");

            $('#'+info[0].id).addClass("active-question");
            // $(".question_number_text").html("Question "+nextNum);

            $(".question").html(info[0].question);

            $(".option1").html(info[1].option);
            $(".option2").html(info[2].option);
            $(".option3").html(info[3].option);
            $(".option4").html(info[4].option);
                         

            // setting option to empty or pre-filled
            if(sessionStorage.getItem(info[0].id)){
                console.log("IFFFFFFF");
                console.log(sessionStorage.getItem(info[0].id));
                var tp = sessionStorage.getItem(info[0].id);
                $('#'+tp).prop("checked", true); 
            }
            else{
                $(".option_one").prop("checked", false);
                $(".option_two").prop("checked", false);
                $(".option_three").prop("checked", false);
                $(".option_four").prop("checked", false);
            }
        }
    })
});



//---------------PREVIOUS CLICK-----------

$("#form").on('click', '#previous', function(){

// sessionStorage.setItem("myVar", "Hello World!");
// console.log(sessionStorage.getItem("myVar"));

var qId = $(".question").get(0).id;
var questionId = qId.split("_")[1];
 
var selectedOptionId = $('input[name=optionRadios]:checked', '#form').val();

var prev = getPreviousId(questionId);
$("#save_input").val(questionId);
$("#next_input").val(questionId);
// var nextNum = $("#save").data('nextnum');

let route = "{{ route('test.fetchQuestions')}}";
$.ajax({
    
    url: route,
    method: 'POST',
    dataType: 'json',
    data:{
        _token: "{{ csrf_token() }}",
        'name': prev
    },
    success: function(info){
        
        // $("#ques").attr('data-value',info[0].id);
        $(".question").attr('id', "question_"+info[0].id);
        $(".option_one").val(info[1].id);
        $(".option_two").val(info[2].id);
        $(".option_three").val(info[3].id);
        $(".option_four").val(info[4].id);

        $(".option_one").attr('id', info[1].id);
        $(".option_two").attr('id', info[2].id);
        $(".option_three").attr('id', info[3].id);
        $(".option_four").attr('id', info[4].id);            

        $(".Question_number_btn").removeClass("active-question");

        $('#'+info[0].id).addClass("active-question");
        // $(".question_number_text").html("Question "+nextNum);

        $(".question").html(info[0].question);

        $(".option1").html(info[1].option);
        $(".option2").html(info[2].option);
        $(".option3").html(info[3].option);
        $(".option4").html(info[4].option);
                     

        // setting option to empty or pre-filled
        if(sessionStorage.getItem(info[0].id)){
            console.log("IFFFFFFF");
            console.log(sessionStorage.getItem(info[0].id));
            var tp = sessionStorage.getItem(info[0].id);
            $('#'+tp).prop("checked", true); 
        }
        else{
            $(".option_one").prop("checked", false);
            $(".option_two").prop("checked", false);
            $(".option_three").prop("checked", false);
            $(".option_four").prop("checked", false);
        }
    }
})

});

</script>

@endsection

