@extends('layouts.app')
@section('title','MCQ-App | Teacher')

@section('content')

<div class="container">

    <h2>Solution Index</h2>
    
    @for ($i = 0; $i < sizeof($attempted_questions); $i++)
        <!-- For Wrong answers -->
        @if (array_key_exists($attempted_questions[$i], $wrong))
            <?php
                $question = App\Question::where('id', $attempted_questions[$i])->first();
                // dd($question);
                $options = App\Option::where('question_id', $attempted_questions[$i])->get();
                // dd($options);
                $correct_option_id;
                for($k=0; $k< sizeof($options); $k++)
                {
                    if($options[$k]->correct == 0)
                    {
                        $correct_option_id = $options[$k]->id;
                    }      
                }
            ?>
            <h4>{{ $question->question }}</h4>
            <div class="option">
                @for ($z = 0; $z < sizeof($options); $z++)
                    @if($options[$z]->id == $wrong[$attempted_questions[$i]])
                        <input class="form-check-input option_one" type="radio" checked>
                        <p class="ml-2 option1 text-danger"> {{ $options[$z]->option }} </p>
                    @elseif($options[$z]->id == $correct_option_id)
                        <input class="form-check-input option_one"   type="radio" checked>
                        <p class="ml-2 option1 text-success"> {{ $options[$z]->option }} </p>
                    @else
                        <input class="form-check-input option_one"   type="radio">
                        <p class="ml-2 option1 "> {{ $options[$z]->option }} </p>
                    @endif
                    
                @endfor
            </div>
        
        @endif

        <!-- For Right answers -->
        @if (array_key_exists($attempted_questions[$i], $right))
            <?php
                $question = App\Question::where('id', $attempted_questions[$i])->first();
                // dd($question);
                $options = App\Option::where('question_id', $attempted_questions[$i])->get();
                // dd($options);
                // $correct_option_id;
                // for($k=0; $k< sizeof($options); $k++)
                // {
                //     if($options[$k]->correct == 0)
                //     {
                //         $correct_option_id = $options[$k]->id;
                //     }      
                // }
            ?>
            <h4>{{ $question->question }}</h4>
            <div class="option">
                @for ($z = 0; $z < sizeof($options); $z++)
                    @if($options[$z]->id == $right[$attempted_questions[$i]])
                        <input class="form-check-input option_one" type="radio" checked>
                        <p class="ml-2 option1 text-success"> {{ $options[$z]->option }} </p>
                    @else
                        <input class="form-check-input option_one"   type="radio">
                        <p class="ml-2 option1"> {{ $options[$z]->option }} </p>
                    @endif
                    
                @endfor
            </div>
        
        @endif

        <br><br>
    @endfor

    @for ($i = 0; $i < sizeof($not_attempted); $i++)
            <!-- For NOT ATTEMPTED -->
            <?php
                $question = App\Question::where('id', $not_attempted[$i])->first();
                // dd($question);
                $options = App\Option::where('question_id', $not_attempted[$i])->get();
                // dd($options);
                $correct_option_id;
                for($k=0; $k< sizeof($options); $k++)
                {
                    if($options[$k]->correct == 0)
                    {
                        $correct_option_id = $options[$k]->id;
                    }      
                }
            ?>
            <h4>{{ $question->question }} <span class="text-danger"><strong> Not Attempted X</strong></span></h4>
            <div class="option">
                @for ($z = 0; $z < sizeof($options); $z++)
                    @if($options[$z]->id == $correct_option_id)
                        <input class="form-check-input option_one"   type="radio" checked>
                        <p class="ml-2 option1 text-danger"> {{ $options[$z]->option }} </p>
                    @else
                        <input class="form-check-input option_one"   type="radio">
                        <p class="ml-2 option1 "> {{ $options[$z]->option }} </p>
                    @endif
                    
                @endfor
            </div>
    @endfor
    
    <form action=" {{ route('result.clearAndGo') }}" method="POST">
        @csrf
        <input type='hidden' name='all_questions_array' value='<?php echo htmlentities(serialize($all_questions)); ?>' />
        <button type="submit"  class="btn btn-primary ">Done</button>
    </form>
</div>
@endsection

