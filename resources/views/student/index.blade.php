@extends('layouts.app')
@section('title','MCQ-App | Student')

@section('content')

<div class="container">

    <!-- Page Heading -->
    <div class="heading">
        <h3>Student Index</h3>
        
        <!-- 
            U will get the test_id from $all_scheduled_tests.
            from that test_id u fetch the test details and will get total_marks of the test
            from that total marks use if else and calculate the duration of the test
        -->

        @foreach ($all_scheduled_tests as $schedule_tests)
            <span>Test id: {{ $schedule_tests->test_id }}</span>
            
            
            <form action=" {{ route('test.schedulingTest') }}" method="POST">
                @csrf
             <?php  

                // $test = Test::where('id', $schedule_tests->test_id)->get();

                $current_time = date('Y-m-d h:i:s', strtotime(Carbon\Carbon::now()->toDateTimeString()));
                $scheduled_time = date('Y-m-d h:i:s', strtotime($schedule_tests->scheduled_time));
                if(($current_time >= $scheduled_time))
                {
                    
                
            ?>
                <input type="hidden" name="schedule_test_id" value="{{ $schedule_tests->test_id }}">
                <button class="btn btn-primary">Start test</button>
            </form>
                    
            <?php
                }
             ?>
             <br>
             <br>

        @endforeach
    </div>

@endsection

