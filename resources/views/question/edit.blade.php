@extends('layouts.app')
@section('title','MCQ-App | Admin')

@section('content')

<div class="container ">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
        <h1 class="h3 mb-4 text-gray-800">Question</h1>
        <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list-ul fa-sm text-white"></i>
        </a>                  
    </div>
  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Create Question</h3></div>
                    <div class="card-body" id="create_question_container">
                        <form action="{{ route('question.update',$question) }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">Subject Name</label>
                                <select name="subjects[]" class="form-control subject_select ">
                                    <option value="{{ $subject->id }}" selected> {{ $subject->name }} </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Chapter Name</label>
                                <select name="chapters[]" class="form-control chapter_select ">
                                    <option value="{{ $chapter->id }}" selected> {{ $chapter->name }} </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Question</label>
                                <input type="text"
                                       id="question"
                                       name="question"
                                       placeholder="Enter question"
                                       value = "{{ $question->question }}"
                                       class="form-control {{ $errors->has('question') ? 'is-invalid' : '' }}">
                                @error('question')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="marks">Marks</label>
                                <input type="text"
                                       id="marks"
                                       name="marks"
                                       placeholder="Enter Marks"
                                       value = "{{ $question->marks }}"
                                       class="form-control {{ $errors->has('marks') ? 'is-invalid' : '' }}">
                                @error('marks')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="name">Difficulty Level</label>
                               
                                    <select name="difficulty_level[]" class="form-control difficulty_select ">
                                @if ($question->difficulty_level == "easy")
                                        <option value="easy" selected>Easy</option>
                                        <option value="medium">Medium</option>
                                        <option value="hard">Hard</option>
                                @elseif($question->difficulty_level == "medium")
                                        <option value="easy" >Easy</option>
                                        <option value="medium" selected>Medium</option>
                                        <option value="hard">Hard</option>
                                @elseif($question->difficulty_level == "hard")
                                        <option value="easy" selected>Easy</option>
                                        <option value="medium">Medium</option>
                                        <option value="hard" selected>Hard</option>
                                @endif
                                    </select>
                                
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="name">Option 1</label>
                                        <input type="text"
                                            id="option1"
                                            name="option1"
                                            placeholder="Enter Option 1"
                                            value = "{{ $options[0]->option }}"
                                            class="form-control {{ $errors->has('option1') ? 'is-invalid' : '' }}">
                                        @error('option1')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 p-4">
                                        <input type="hidden" name="option1_id" value="{{ $options[0]->id }}">
                                        <input class="form-check-input"  name="optionRadios" type="radio" value="1" id="radio1">
                                        <label class="form-check-label" for="radio1">
                                            Correct
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="name">Option 2</label>
                                        <input type="text"
                                            id="option2"
                                            name="option2"
                                            placeholder="Enter Option 2"
                                            value = "{{ $options[1]->option }}"
                                            class="form-control {{ $errors->has('option2') ? 'is-invalid' : '' }}">
                                        @error('option2')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 p-4">
                                        <input type="hidden" name="option2_id" value="{{ $options[1]->id }}">
                                        <input class="form-check-input" name="optionRadios"  type="radio" value="2" id="radio2">
                                        <label class="form-check-label" for="radio2">
                                            Correct
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="name">Option 3</label>
                                        <input type="text"
                                            id="option3"
                                            name="option3"
                                            placeholder="Enter Option 3"
                                            value = "{{ $options[2]->option }}"
                                            class="form-control {{ $errors->has('option3') ? 'is-invalid' : '' }}">
                                        @error('option3')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 p-4">
                                        <input type="hidden" name="option3_id" value="{{ $options[2]->id }}">
                                        <input class="form-check-input" name="optionRadios"  type="radio" value="3" id="radio3">
                                        <label class="form-check-label" for="radio3">
                                            Correct
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="name">Option 4</label>
                                        <input type="text"
                                            id="option4"
                                            name="option4"
                                            placeholder="Enter Option 4"
                                            value = "{{ $options[3]->option }}"
                                            class="form-control {{ $errors->has('option4') ? 'is-invalid' : '' }}">
                                        @error('option4')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-2 p-4">
                                        <input type="hidden" name="option4_id" value="{{ $options[3]->id }}">
                                        <input class="form-check-input" name="optionRadios"  type="radio" value="4" id="radio4">
                                        <label class="form-check-label" for="radio4">
                                            Correct
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-success">Update Question</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      
</div>
@endsection

