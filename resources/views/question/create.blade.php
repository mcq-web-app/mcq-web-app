@extends('layouts.app')
@section('title','MSBTE | Teacher')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <!-- Breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('teacher.index') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('subjects.index') }}">Subjects</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('subjects.show', $subject->id) }}">{{ $subject->name }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Questions</li>
                </ol>
            </nav>
        </div>

        <div class="col-md-12">
            <!-- Page Heading -->
            <div class="heading">
                <h3>Add Question</h3>
            </div>
        </div>

        <div class="col-md-12">
            <form action="{{ route('questions.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Subject Name</label>
                    <select name="subjects[]" class="form-control subject_select " readonly>
                    <!-- <option disabled selected>Select Subject</option> -->

                        <option value="{{ $subject->id }}" > {{ $subject->name }} </option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Chapter Name</label>
                    <select name="chapters[]" class="form-control chapter_select " readonly>
                        <!-- <option disabled selected>Select Chapter</option> -->
                        <option value="{{ $chapter->id }}" > {{ $chapter->name }} </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Question</label>
                    <input type="text"
                           id="question"
                           name="question"
                           placeholder="Enter question"
                           value = "{{ old('question') }}"
                           class="form-control {{ $errors->has('question') ? 'is-invalid' : '' }}">
                    @error('question')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="marks">Marks</label>
                    <input type="text"
                           id="marks"
                           name="marks"
                           placeholder="Enter Marks"
                           value = "{{ old('marks') }}"
                           class="form-control {{ $errors->has('marks') ? 'is-invalid' : '' }}">
                    @error('marks')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Difficulty Level</label>
                    <select name="difficulty_level[]" class="form-control difficulty_select ">
                        <option disabled selected>Select DIfficulty level</option>
                        <option value="easy">Easy</option>
                        <option value="medium">Medium</option>
                        <option value="hard">Hard</option>
                    </select>
                </div>
                <!-- options -->

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            <label for="name">Option 1</label>
                            <input type="text"
                                id="option1"
                                name="option1"
                                placeholder="Enter Option 1"
                                value = "{{ old('option1') }}"
                                class="form-control {{ $errors->has('option1') ? 'is-invalid' : '' }}">
                            @error('option1')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-2 p-4">
                            <input class="form-check-input"  name="optionRadios" type="radio" value="1" id="radio1">
                            <label class="form-check-label" for="radio1">
                                Correct
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            <label for="name">Option 2</label>
                            <input type="text"
                                id="option2"
                                name="option2"
                                placeholder="Enter Option 2"
                                value = "{{ old('option2') }}"
                                class="form-control {{ $errors->has('option2') ? 'is-invalid' : '' }}">
                            @error('option2')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-2 p-4">
                            <input class="form-check-input" name="optionRadios"  type="radio" value="2" id="radio2">
                            <label class="form-check-label" for="radio2">
                                Correct
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            <label for="name">Option 3</label>
                            <input type="text"
                                id="option3"
                                name="option3"
                                placeholder="Enter Option 3"
                                value = "{{ old('option3') }}"
                                class="form-control {{ $errors->has('option3') ? 'is-invalid' : '' }}">
                            @error('option3')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-2 p-4">
                            <input class="form-check-input" name="optionRadios"  type="radio" value="3" id="radio3">
                            <label class="form-check-label" for="radio3">
                                Correct
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-10">
                            <label for="name">Option 4</label>
                            <input type="text"
                                id="option4"
                                name="option4"
                                placeholder="Enter Option 4"
                                value = "{{ old('option4') }}"
                                class="form-control {{ $errors->has('option4') ? 'is-invalid' : '' }}">
                            @error('option4')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-2 p-4">
                            <input class="form-check-input" name="optionRadios"  type="radio" value="4" id="radio4">
                            <label class="form-check-label" for="radio4">
                                Correct
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-outline-success">Add Question</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

