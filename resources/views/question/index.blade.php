@extends('layouts.app')
@section('title','MCQ-App | Teacher')


@section('content')

<div class="container">

    <!-- Breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('teacher.index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('subjects.index') }}">Subjects</a></li>
            <li class="breadcrumb-item"><a href="{{ route('subjects.show', $subject->id) }}">{{ $subject->name }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Questions</li>
        </ol>
    </nav>

    <!-- Page Heading -->
    <div class="heading">
        <form action="{{ route('question.create', [$subject, $chapter]) }}" method="POST">
            @csrf
            @method('PUT')
            <h3 class="d-inline-block">All Questions</h3>
            <button type="submit" class="btn btn-primary text-white float-right">
                <span class="fas fa-plus"></span> Add Question
            </button>
        </form>
    </div>

    <table class="table table-bordered" id="manage-questions-table">
        <thead>
            <tr>
                <th>Question</th>
                <th>Marks</th>
                <th>Difficulty Level</th>
                <th>Subject</th>
                <th>Chapter</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($questions as $question)
            <?php
                $subject = App\Subject::where('id', $question->subject_id)->get()[0];
                $chapter = App\Chapter::where('id', $question->chapter_id)->get()[0];
            ?>
            <tr class="{{ $question->id }}">
                <td>{{$question->question}}</td>
                <td>{{$question->marks}}</td>
                <td>{{$question->difficulty_level}}</td>
                <td>{{$subject->name}}</td>
                <td>{{$chapter->name}}</td>
                <td>
                    <button class="btn btn-sm btn-primary" data-info="">
                        <span class=""></span> View
                    </button>
                    <!-- <button class="edit-modal btn btn-sm btn-secondary" data-info="">
                        <span class=""></span> Edit
                    </button> -->
                    <a href="{{ route('question.edit', $question->id) }}" class="btn edit-modal btn btn-sm btn-secondary">
                        Edit
                    </a>
                    <button class="delete-modal btn btn-sm btn-danger" data-info="">
                        <span class=""></span> Delete
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
@endsection

@section('page-level-scripts')

@endsection
