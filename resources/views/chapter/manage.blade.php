@extends('layouts.admin')
@section('title','MCQ-App | Admin')

@section('chapters','active')
@section('chapters-show','show')
@section('chapter-manage','active')

@section('content')

<div class="container">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">All Chapters</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Chapters</h6>
        </div>
        <div class="card-body">
            <div id="export-buttons" class="float-right"></div>
            <table class="table table-hover table-bordered">
                <thead class="thead-primary">
                    <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Name</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($chapters as $chapter)
                        <tr>
                            <th scope="row">{{ $chapter->id }}</th>
                            <td>{{ $chapter->name }}</td>

                            <td>{{ $chapter->subject_id }}</td>

                            <td class="mr-3">
                                <button class="btn btn-primary"><i class="fas fa-pen"></i> Edit</button>
                                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection
