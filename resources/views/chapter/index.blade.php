@extends('layouts.layout')
@section('title','MCQ-App | Admin')

@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
        <h1 class="h3 mb-4 text-gray-800">All chapters</h1>
        <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list-ul fa-sm text-white"></i>
        </a>
    </div>

    <!-- Add Chapter -->
    <form action="{{ route('chapter.create', $subject_id) }}" method="POST">
        @csrf
        @method('PUT')
        <button type="submit" class="btn btn-link btn-primary text-white my-3">
            <span class="fas fa-plus"></span>
             Add Chapter
        </button>
    </form>

</div>
@endsection
