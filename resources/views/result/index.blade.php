@extends('layouts.app')
@section('title','MCQ-App | Student')

<?php
    $status = "";
    $percentage = ($total_marks / $total_test_marks)*100;
    if($percentage >= 40)
    {   
        $status = "Pass !";
?>
<?php
    }
    else{
        $status = "Fail !";
?>
<?php
    }
?>

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-8 result-card">
            <div class="card">
                <div class="card-body card-content">
                    <div class="text-center">
<?php
if($percentage >= 40)
{
?>
                        <h2 class="success-heading">Congratulations!</h2>
<?php
}else{
?>
                        <h2 class="fail-heading">Better Luck Next Time!</h2>
<?php
}
?>
                    <p class="tagline">
                    You scored <span class="marks-obtained">{{ $total_marks }}</span> out of <span class="total-marks">{{ $total_test_marks }}</span> in <span class="subject">{{ $subj_name }}</span>
                    </p>
<?php
if($percentage >= 40)
{
?>
                        <h2 class="text-success"> <strong>PASS !</strong> </h2>
<?php
}else{
?>
                        <h2 class="text-danger"> <strong>FAIL !</strong> </h2>
<?php
}
?>
                        <form action=" {{ route('result.clearAndGo') }}" method="POST">
                            @csrf
                            <input type='hidden' name='all_questions_array' value='<?php echo htmlentities(serialize($all_questions)); ?>' />
                            <button type="submit"  class="btn btn-outline-primary ">Done</button>
                        </form>
                        
                        <form action="{{ route('solution.index') }}" method="POST">
                            @csrf
                            <input type='hidden' name='all_questions_array' value='<?php echo htmlentities(serialize($all_questions)); ?>' />
                            <button type="submit"  class="btn btn-outline-primary ">view solution</button>
                        </form>
                    </div>
                </div>
                <a href="#" class="all-questions">View All Question</a>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        sessionStorage.clear();
    </script>
@endsection


