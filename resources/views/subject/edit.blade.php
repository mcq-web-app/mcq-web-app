@extends('layouts.layout')
@section('title','MCQ-App | Admin')

@section('main-content')

<div class="container mt-4 mx-2">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between">
        <h1 class="h3 mb-4 text-gray-800">Subject</h1>
        <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list-ul fa-sm text-white"></i>
        </a>                  
    </div>
  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Update Subject</span></h3></div>
                    <div class="card-body">
                        <form action="{{ route('subjects.update', $subject->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Subject Name</label>
                                <input type="text"
                                       id="name"
                                       name="name"
                                       placeholder="Enter Name"
                                       value = "{{ $subject->name }}"
                                       class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                @error('name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-success">Update Subject</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      
</div>
@endsection