@extends('layouts.admin')
@section('title','MCQ-App | Admin')

@section('subjects','active')
@section('subjects-show','show')
@section('subject-manage','active')

@section('main-content')

<div class="container">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">All Subjects</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Subjects</h6>
        </div>
        <div class="card-body">
            <div id="export-buttons" class="float-right"></div>
            <table class="table table-hover table-bordered">
                <thead class="thead-primary">
                    <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subjects as $subject)
                        <tr>
                            <th scope="row">{{ $subject->id }}</th>
                            <td>{{ $subject->name }}</td>
                            <td class="mr-3">
                                <button class="btn btn-primary"><i class="fas fa-pen"></i> Edit</button>
                                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection
