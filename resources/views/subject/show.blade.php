@extends('layouts.app')
@section('title','MCQ-App | Teacher')

@section('content')

<div class="container">
    <div class="chapters-list">
        <div class="row">
            <div class="col-md-12">

                <!-- Breadcrumb -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('teacher.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('subjects.index') }}">Subjects</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $subject->name }}</li>
                    </ol>
                </nav>

                <!-- Page Heading -->
                <div class="heading">
                    <h3>{{ $subject->name }}</h3>
                </div>

                @if (auth()->user()->isStudent())
                
                @endif

                <form action="{{ route('test.store') }}" method="POST">
                    @csrf
                    <table class="table">
                        <tbody>
                            <input type="text"value="{{ $chapters[0]->subject_id }}" name="subject_id" hidden>
                            @foreach($chapters as $chapter)
                                <tr>
                                    <td>
                                        {{ $chapter->name }}
                                        @if (auth()->user()->role == "teacher")
                                            <a href="{{ route('question.index_specific', [$chapter->id, $subject->id]) }}" class="btn btn-primary btn-sm text-white float-right ml-4">
                                                <span class="fas fa-eye"></span> View All Questions
                                            </a>
                                        @endif
                                        
                                        <div class="form-check float-right" id="outer">
                                            <input class="form-check-input checkbox" type="checkbox" name="checkbox_{{$chapter->id}}" id="checkbox_{{$chapter->id}}" onclick="getSelectedCheckBox()">
                                            <input class="form-control weightAge" id="{{$chapter->id}}" name="{{$chapter->id}}" type="number" value="0" style="display: none;"  onclick="marksChanged()" oninput="marksChanged()" min=0>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="form-group">
                        <label for="totalMarks">Total Marks</label>
                        <input type="number" class="form-control" name="totalMarks" id="totalMarks" value="0" readonly>
                    </div>

                    @if (auth()->user()->isTeacher())
                        <div class="form-group">
                            <label for="scheduleDate">Schedule On</label>
                            <input type="datetime-local" class="form-control" name="scheduleDate" id="scheduleDate">
                        </div>
                    @endif

                    <button type="submit" class="btn btn-primary text-white submitBtn" id="StartTestBtn">
                        <span class="fas fa-eye"></span>
                        @if (auth()->user()->isTeacher())
                            Schedule Test
                        @else
                            Start Practise Test
                        @endif

                    </button>
                </form>
               

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

    document.getElementById('StartTestBtn').disabled = true;

    var total_field = document.getElementById('totalMarks')
    var checkboxes = document.getElementsByClassName('checkbox');
    var weightAges = document.getElementsByClassName('weightAge');
    var checkBoxSelected = 0;
    var sum = 0;
function getSelectedCheckBox(){
    var selected = [];
    for (var i=0; i<checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            selected.push(checkboxes[i].id);
            weightAges[i].style.display = 'inline-block';
        }else{
            weightAges[i].style.display = 'none';
        }
        checkBoxSelected = selected.length;
    }

    for(var j=0; j<selected.length;j++)
    {
        // console.log(selected[j]);
        var x = selected[j].split("_")[1];
        console.log("id = "+x);

        var marks_ele = document.getElementById(x);
        console.log("marks = "+ marks_ele.value);
    }
    checkSubmitBtn();
}
$("input:checkbox.checkbox").click(function() {
    var ele_id;
    if(!$(this).is(":checked"))
    {
        ele_id = this.id;
        var x = ele_id.split("_")[1];
        var marks_ele = document.getElementById(x);
        console.log("sum: "+sum);
        sum -= parseInt(marks_ele.value);
        marks_ele.value = 0;
        total_field.value = "";
        total_field.value = sum;

        // alert('you are unchecked ' + marks_ele.value);
    }
    checkSubmitBtn();
});

function marksChanged(){

    var selected = [];
    for (var i=0; i<checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            selected.push(checkboxes[i].id);
            weightAges[i].style.display = 'inline-block';
        }else{
            weightAges[i].style.display = 'none';
            console.log("Unchecked !");
        }
        checkBoxSelected = selected.length;
    }

    sum = 0;
    for(var k=0; k<checkBoxSelected;k++){
        var id = selected[k].split("_")[1];
        // console.log("length = "+selected.length);
        var marks_ele = document.getElementById(id);
        console.log("marks = "+marks_ele.value);
        if(marks_ele.value != "")
            sum += parseInt(marks_ele.value);
    }
    total_field.value = "";
    console.log("sum = "+sum);
    total_field.value = sum;
    checkSubmitBtn();
}

function checkSubmitBtn(){
    var submitBtn = document.getElementById('StartTestBtn');
    if(total_field.value != "0")
    {
        console.log("-----Checking--------");
        submitBtn.disabled  = false;
        console.log("do false");
    }else{
        submitBtn.disabled  = true;
        console.log("do true");
    }
    console.log("Total marks == "+total_field.value);
}



</script>

@endsection
