@extends('layouts.app')
@section('title','MCQ-App | Teacher')

@section('content')

<div class="container">

    <!-- Breadcrumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('teacher.index') }}">Home</a></li>
            <li class="breadcrumb-item" aria-current="page">Subjects</li>
        </ol>
    </nav>

    <!-- Page Heading -->
    <div class="heading">
        <h3>Subjects</h3>
    </div>

    <div class="row">
        @foreach($subjects as $subject)
            <div class="col-sm-4">
                <div id="subject-card" class="card border-primary text-center">
                    <div class="card-body text-primary">
                        <a href="{{ route('subjects.show', $subject->id) }}" class="stretched-link"><h5 class="card-title">{{ $subject->name }}</h5></a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
@endsection

