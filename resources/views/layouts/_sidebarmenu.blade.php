<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MSBTE</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="#">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Subjects Collapse Menu -->
    <li class="nav-item @yield('subjects')">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSubjects" aria-expanded="true" aria-controls="collapseSubjects">
            <i class="fas fa-fw fa-book"></i>
            <span>Subjects</span>
        </a>
        <div id="collapseSubjects" class="collapse @yield('subjects-show')" aria-labelledby="headingSubjects" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('subject-create')" href="{{ route('subjects.create') }}">Add Subject</a>
                <a class="collapse-item @yield('subject-manage')" href="{{ route('subject.manage') }}">Manage Subject</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Chapters Collapse Menu -->
    <li class="nav-item @yield('chapters')">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseChapters" aria-expanded="true" aria-controls="collapseChapters">
            <i class="fas fa-fw fa-list"></i>
            <span>Chapters</span>
        </a>
        <div id="collapseChapters" class="collapse @yield('chapters-show')" aria-labelledby="headingChapters" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('chapter-create')" href="{{ route('chapters.create') }}">Add Chapter</a>
                <a class="collapse-item @yield('chapter-manage')" href="{{ route('chapter.manage') }}">Manage Chapter</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

</ul>
<!-- End of Sidebar -->
