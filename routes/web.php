<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::middleware(['auth'])->group(function(){

    Route::get('/home', 'HomeController@index')->name('home');
    //Admin
    Route::get('/admin', 'AdminController@index')->name('admin.index');
    //Teacher
    Route::get('/teacher', 'TeacherController@index')->name('teacher.index');
    //Student
    Route::get('/student', 'StudentsController@index')->name('student.index');

    //Test
    Route::get('/test', 'TestsController@index')->name('test.index');
    Route::post('/test/store', 'TestsController@store')->name('test.store');
    Route::post('/test/started', 'TestsController@fetchQuestions')->name('test.fetchQuestions'); //ajax
    Route::post('test/scheduledTest', 'TestsController@schedulingTest')->name('test.schedulingTest');

    //Result
    Route::post('/result', 'ResultController@index')->name('result.index');
    Route::post('/result/clear', 'ResultController@clearSessionAndGoToHome')->name('result.clearAndGo');


    //Subjects
    Route::resource('subjects', 'SubjectsController');
    Route::put('subjects/{subject}/edit', 'SubjectsController@edit')->name('subject.edit');
    Route::put('subjects/{subject}/update', 'SubjectsController@update')->name('subject.update');
    Route::get('/manage-subjects', 'SubjectsController@manage')->name('subject.manage');

    //Chapters
    Route::resource('chapters', 'ChaptersController');
    Route::put('subjects/{subject}/chapters', 'ChaptersController@index')->name('chapter.index');
    Route::get('chapters/{chapter}/edit', 'ChaptersController@edit')->name('chapter.edit');
    Route::put('chapters/{chapter}/update', 'ChaptersController@update')->name('chapter.update');
    Route::put('chapters/{subject}/create', 'ChaptersController@create')->name('chapter.create');
    Route::get('/chapters', 'ChaptersController@manage')->name('chapter.manage');

    //Questions
    Route::resource('questions', 'QuestionsController');
    Route::get('questions/chapters/{subject}', 'QuestionsController@getChapters'); // for ajax
    Route::get('questions/{question}/edit', 'QuestionsController@edit')->name('question.edit');
    Route::post('questions/{question}/update', 'QuestionsController@update')->name('question.update');
    Route::get('chapters/{chapter}/{subject}', 'QuestionsController@index_specific')->name('question.index_specific');
    Route::put('questions/create/{subject}/{chapter}', 'QuestionsController@create')->name('question.create');

    //options
    Route::resource('options', 'OptionsController');
    Route::post('/options/correct', 'OptionsController@selectCorrectOption')->name('options.selectCorrect');

    //solution
    Route::post('/test/solution', 'SolutionController@index')->name('solution.index');
});
