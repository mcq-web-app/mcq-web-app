<?php

use Illuminate\Database\Seeder;

class ChapterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Subject 1
        \App\Chapter::create([
            'name'=> 'Introduction to Software Engineering',
            'subject_id'=> 1
        ]);
        \App\Chapter::create([
            'name'=> 'Models in SEN',
            'subject_id'=> 1
        ]);
        \App\Chapter::create([
            'name'=> 'TESTING AND MAINTENANCE',
            'subject_id'=> 1
        ]);

        //SUbject 2
        \App\Chapter::create([
            'name'=> 'Introduction to 8086 Architecture',
            'subject_id'=> 2
        ]);
        \App\Chapter::create([
            'name'=> 'Instruction Set and Programming',
            'subject_id'=> 2
        ]);
        \App\Chapter::create([
            'name'=> 'Instruction Set and Programming',
            'subject_id'=> 2
        ]);

        //subject 3
        \App\Chapter::create([
            'name'=> 'Introduction to Java Basics',
            'subject_id'=> 3
        ]);
        \App\Chapter::create([
            'name'=> 'Working with Data Structures',
            'subject_id'=> 3
        ]);
        \App\Chapter::create([
            'name'=> 'Start with JDBC',
            'subject_id'=> 3
        ]);

        //subject 4
        \App\Chapter::create([
            'name'=> 'Introduction to Programming',
            'subject_id'=> 4
        ]);
        \App\Chapter::create([
            'name'=> 'Introduction to compiler , Linker and loader',
            'subject_id'=> 4
        ]);
        \App\Chapter::create([
            'name'=> 'Constants, Variables and Data types in C',
            'subject_id'=> 4
        ]);

    }
}
