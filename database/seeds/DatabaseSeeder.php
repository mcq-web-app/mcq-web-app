<?php

use App\Subject;
use App\Chapter;
use App\Option;
use App\Question;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(ChapterSeeder::class);

        // factory(App\Question::class, 120)->create()->make();

        factory(Question::class, 30)->create()
            ->each(function($question){
                $question->options()
                    ->saveMany(factory(Option::class, 4)->make());
        });

    }
}
