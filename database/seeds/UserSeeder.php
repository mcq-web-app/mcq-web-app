<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=> 'Jhon Doe',
            'email'=> 'jhon@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('jhondoe'),
            'role'=> 'admin'
        ]);

        \App\User::create([
            'name'=> 'Yash Thakkar',
            'email'=> 'yashthakkar2700@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'student'
        ]);

        \App\User::create([
            'name'=> 'Raj Panchal',
            'email'=> 'raj@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('rajpanchal'),
            'role'=> 'student'
        ]);

        \App\User::create([
            'name'=> 'Nick',
            'email'=> 'nick@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'teacher'
        ]);

        \App\User::create([
            'name'=> 'Jimmy Doe',
            'email'=> 'jimmy@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'student'
        ]);

        \App\User::create([
            'name'=> 'Jane Doe',
            'email'=> 'jane@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('yashthakkar'),
            'role'=> 'student'
        ]);

    }
}
