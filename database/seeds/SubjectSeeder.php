<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Subject::create([
            'name'=> 'Software Engineering'
        ]);

        \App\Subject::create([
            'name'=> 'Microprocessor'
        ]);

        \App\Subject::create([
            'name'=> 'Java'
        ]);

        \App\Subject::create([
            'name'=> 'C Programming'
        ]);
    }
}
