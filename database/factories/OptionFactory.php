<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Factory;
use Faker\Generator as Faker;

$factory->define(App\Option::class, function (Faker $faker) {
    return [
        'question_id' => rand(1, 30),
        'subject_id' => 1,//rand(1, 4),
        'chapter_id' => rand(1,3),
        'option' => $faker->sentence(rand(4,6)),
        'correct' => 0
    ];
});
