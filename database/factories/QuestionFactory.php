<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Illuminate\Support\Facades\DB;
use App\Question;
use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence(rand(4,8)),
        'chapter_id' => rand(1,3),
        'subject_id' => 1,//rand(1, 4),
        'marks' => rand(1,2)
    ];
});
